// pour creer un boutton , ID du boutton , nouvelle location
function clickToLocation(id, newLoc) {
    $(id).click(function () {
        location.hash = newLoc;
    });
}
// render un template et retourne la str qui contient l'affichage généré par le template
function renderTpl(tpl, data) {
    var template = $.templates(tpl);
    return template.render(data);
}
// render un template et l'affiche dans l'élément renseigné par l'id
function renderTplTo(tpl, data, loc) {
    $(loc).html(renderTpl(tpl, data));
}
// remplir l'id du select avec les jours dans le mois
function fillDays(id) {
    var htmlContent = "";
    for (var i = 1; i < 32; ++i) {
        htmlContent += '<option value="' + i + '">' + i + '</option>';
    }
    $(id).html(htmlContent);
}
// pareil pour les mois
function fillMonths(id) {
    var month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
        htmlContent = "";
    for (var i = 0; i < 12; ++i) {
        htmlContent += '<option value="' + (i + 1) + '">' + month[i] + '</option>';
    }
    $(id).html(htmlContent);
}
// remplis un formulaire de selects de birthDay (id automatique) avec les bonnes options
function fillBirthDayForm() {

    fillDays("#birthDay");
    fillMonths("#birthMonth");

    var today = new Date(),
        todayYear = today.getFullYear(),
        htmlContent = "";
    for (var i = todayYear; i > todayYear - 100; --i) {
        htmlContent += '<option value="' + i + '">' + i + '</option>';
    }
    $("#birthYear").html(htmlContent);
}
// remplir un formulaire de selects de dates pour le futur (remplir la date d'un projet futur par ex)
function fillFutureDate(dayId, monthId, yearId) {
    var today = new Date(),
        todayYear = today.getFullYear(),
        htmlContent = "";

    fillDays(dayId);
    fillMonths(monthId);

    htmlContent = "";
    for (var i = todayYear; i < todayYear + 30; ++i) {
        htmlContent += '<option value="' + i + '">' + i + '</option>';
    }
    $(yearId).html(htmlContent);
}
// fonction plus élaborée de remplissage de date, par ex 30 , 50 pour les jumps signifie que la date ira d'il y à 30 ans à dans 50 ans (pour 2018 on pourra choisir une date entre 1988 et 2068)
function fillDate(id, yearLowJump, yearHighJump) {
    var today = new Date(),
        todayYear = today.getFullYear(),
        htmlContent = "";
    fillDays("#" + id + "Day");
    fillMonths("#" + id + "Month");

    for (var i = todayYear - yearLowJump; i < todayYear + yearHighJump; ++i) {
        htmlContent += '<option value="' + i + '">' + i + '</option>';
    }
    $("#" + id + "Year").html(htmlContent);
}
// rajoute l'array des langues à l'array data fourni
function addLangToData(data) {

    return $.extend({}, lang[sessionData["lang"]], data);
}
// pour fusionner 2 tableaux
function arrayMerge(array1, array2) {
    return $.extend({}, array1, array2);
}
// récupérer l'array de langue actuel dépendant de la langue choisie par l'utilisateur
function getLang() {
    return lang[sessionData["lang"]];
}

// récupère le dernier id numéroté (si on a id1 id2 id3 on fait getLastId("id",1) et ça retourne 3)
function getLastId(id, start) {
    for (var i = start; $("#" + id + i).length; ++i);
    return i - 1;
}
// pour creer un formulaire ou l'user choisi des langues et indique son niveau dans la langue en question (A OPTIMISER)
function languageSkillMaker(parentId, childId) {
    $("#" + childId + getLastId(childId, 1)).change(function () {
        // on rajoute une ligne
        var data = {
            "id": childId + (getLastId(childId, 1) + 1),
            "options": [
                { "value": "en", "option": "English" },
                { "value": "fr", "option": "French" }
            ]
        };
        $("#" + parentId).append(renderTpl("#multipleLanguagesWithLevelSelectRow", data));

        $('#' + childId + getLastId(childId, 1) + 'Slider').bootstrapSlider({
            max: 3,
            min: 0,
            step: 1,
            formatter: function (val) {
            }
        }).on('change', function (data) {
        });
        languageSkillMaker(parentId, childId);
    });
}



/*********************
 * pour les input text qui se reproduisent chaque fois qu'on rajotue des trucs
 * https://stackoverflow.com/questions/13800298/how-can-i-automatically-generate-a-new-input-field-when-a-certain-input-field-is
 * http://jsfiddle.net/jCMc8/8/
 * *******************/

function addRow(section, initRow) {
    var newRow = initRow.clone().removeAttr('id').addClass('new').insertBefore(initRow),
        deleteRow = $('<div class="col-md-1"><a class="rowDelete"><img src="http://i.imgur.com/ZSoHl.png"></a></div>');

    newRow
        .append(deleteRow)
        .on('click', 'a.rowDelete', function () {
            removeRow(newRow, initRow);
        })
        .slideDown(300, function () {
            $(this)
                .find('input').focus();
        })
}

function removeRow(newRow, initRow) {
    newRow
        .slideUp(200, function () {
            $(this)
                .next('div:not(#' + initRow.attr("id") + ')')
                .find('input').focus()
                .end()
                .end()
                .remove();
        });
}

function createMultiField(initRowId) {

    var initRow = $('#' + initRowId),
        section = initRow.parent('section');

    initRow.on('focus', 'input', function () {
        addRow(section, initRow);
    });
}

function generatePageSystem(arr) {
    var aSpeed = 200;

    for (var k in arr) {
        $("#" + arr[k]).hide();
        $("#" + arr[k] + "Button").click(function () {
            for (var key in arr) {
                if (($(this).attr("id")).slice(0, -6) == arr[key]) {
                    $("#" + arr[key]).delay(aSpeed).fadeIn(aSpeed);
                } else {
                    $("#" + arr[key]).fadeOut(aSpeed);
                }
            }

        });
    }
}

function isEmpty(str) {
    return (!str || 0 === str.length);
}

function roleStrToInt(str) {
    var roleId = -1;
    $.each(commonData.roles,function(k,v){
        if(str == v.name){
            roleId = parseInt(k);
            return false;
        }
    })
    if(roleId > 0){
        return roleId;
    } else {
        console.log("Critical error : the roleName couldn't be parsed to an ID : " , str);
        return -1;
    }
    /*
    switch (str) {
        case "Admin": return 1;
        case "Customers": return 2;
        case "Consultants": return 3;
        case "NewCandidates": return 4;
    }
    */
}
function alertSessionData() {
    alert(JSON.stringify(sessionData));
}
// thx W3Cschool
function setCookie(cname, cvalue, exmin) {
    var d = new Date();
    d.setTime(d.getTime() + (exmin * 60));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    console.log("setting cookie : " + cname + " -> " + cvalue);
}
// thx again
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            var val = c.substring(name.length, c.length);
            console.log("Got cookie : " + cname + " -> " + val);
            return val;
        }
    }
    console.log("Didn't find cookie");
    return "";
}
// to be called whenever we get or recover a token
function setRequestHeader(tokenType, token) {
    var header = tokenType + " " + token;
    $.ajaxSetup({
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', header);
        }
    });
    console.log("Auhorization SET :" + header);
}
function ajaxGet(page) {
    return $.ajax({
        url: apiRoot + page, // login treatment url
        dataType: 'json',
        type: 'get',
        contentType: 'application/json',
        //data:data,
    }).fail(function (res) {
        console.log("Ajax GET request failed");
        console.log(res);
        if (res === Object(res)) {
            console.log("-----------------------------------------------------------");
            console.log(page, " get request failed");
        }
    });
}
function ajaxPost(page, data) {
    return $.ajax({
        url: apiRoot + page, // login treatment url
        dataType: 'json',
        type: 'post',
        contentType: 'application/x-www-form-urlencoded',
        data: data
    }).fail(function (res) {
        console.log("Ajax POST request failed with data (" + page + ") :");
        console.log(data);
        if (res === Object(res)) {
            if ("responseJSON" in res) {
                alert(res.responseJSON.Message);
            } else {
                console.log("-----------------------------------------------------------");
                console.log(page, " post request failed : ", res);
            }
        }
    });
}
function ajaxDelete(page, data) {
    return $.ajax({
        url: apiRoot + page, // login treatment url
        dataType: 'json',
        type: 'delete',
        contentType: 'application/x-www-form-urlencoded',
        data: data
    }).fail(function (res) {
        console.log("-----------------------------------------------------------");
        console.log("Ajax delete request failed with data (" + page + ") :");
        console.log(data);
        console
    });
}
function ajaxPut(page, data) {
    return $.ajax({
        url: apiRoot + page, // login treatment url
        dataType: 'json',
        type: 'put',
        contentType: 'application/json',
        data: data
    }).fail(function (res) {
        console.log("-----------------------------------------------------------");
        console.log("Ajax PUT request failed with data (" + page + ") :");
        console.log(data);
        if (res === Object(res)) {
            if ("responseJSON" in res) {
                alert(res.responseJSON.Message);
            }
        }
    });
}
function sessionTimeout(expires) {

    var firstWarningTime = (expires > 600) ? (expires - 300) : 30;
    setTimeout(function () {
        //alert("Warning, you will be disconnected soon");
    }, firstWarningTime);

    setTimeout(function () {
        //alert("You have been disconnected");
        //location.hash = "#login";
    }, expires);
}
// 
function login(mail, password) {
    return $.ajax({
        url: apiRoot + 'token', // login treatment url
        dataType: 'json',
        type: 'post',
        contentType: 'application/x-www-form-urlencoded',
        data: {
            "username": mail,
            "password": password,
            "grant_type": "password"
        }
    }).done(function (data, textStatus, jQxhr) {
        var result = {
            token: data.access_token,
            tokenType: data.token_type,
            expires: data.expires_in
        };
        sessionData.token = result;
        sessionTimeout(result.expires);

    }).fail(function (jqXhr, textStatus, errorThrown) {
        alert("error");
    });
}
// first char to lower case
function fctlc(str) {
    return str.charAt(0).toLowerCase() + str.slice(1);
}
function fctup(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
}
function firstCharToLowerCase(str) {
    return fctlc(str);
}
function firstCharToUpperCase(str) {
    return fctup(str);
}
function generateDefaultTemplate(input) {
    var defaultTemplate;
    for (var k in input) {
        switch (input[k]) {

        }
    }
}

// token header must be set

function getUserLevel() {
    console.log("userlevel is called");
    return ajaxGet('api/values')
        .done(function (data) {
            console.log("success userlevel");
            for (var obj of data) {
                if (obj.type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/role") {
                    sessionData.userLevel = roleStrToInt(obj.value);
                }
            }
        })
        .fail(function () {
            alert("failure userlevel");
        });
}
// performs login with mail/password and gets token & userlevel
function performLoginAction(mail, password, onSuccess) {
    return login(mail, password)
        .done(function () {
            //alert("Setting request header with :" + JSON.stringify(sessionData.token));
            setRequestHeader(sessionData.token.tokenType, sessionData.token.token);
            sessionData.mail = mail;
            getUserLevel().done(function () {
                alert("login action success");
                recoverDataToCookies();
                onSuccess();
            }).fail(function () {
                alert("login action failed");
                alertSessionData();
            })
        }).fail(function () {
            alert("Login failed");
        });
}
// creates recovery cookies
function recoverDataToCookies() {
    setCookie("mail", sessionData.mail, 999999);
    setCookie("token", JSON.stringify(sessionData.token), 999999);
}
// when the user presses F5, all variables are lost, critical data (token and mail) are stored in cookies to allow us to stay connected
function tryRecoverDataFromCookies(onSuccess, onFail) {
    //console.log("attempting recover from : " + document.cookie);
    var mail = getCookie("mail");
    if (mail != "") {
        console.log("successfuly recovered mail");
        var token = JSON.parse(getCookie("token"));
        //console.log("token to be used for recovery :" + JSON.stringify(token));
        setRequestHeader(token.tokenType, token.token);
        return getUserLevel()
            .done(function () {
                sessionData.mail = mail;
                sessionData.token = token;
                sessionTimeout(token.expires);
                onSuccess();
            }).fail(function () {
                console.log("recover failed to recover userLevel");
                onFail();
            });
    } else {
        onFail();
    }
}

// allows us to formate a data array (most likely retrieved from a webservice) to something that we can use (for our templates for exemple)

/*
lets say we have an input array that contains :
[
    {"level":5,"power":10},
    {"level":2,"power":3}
]
and we want something like :
[
    {"lvl":5,"displayedLevel":5,"pwr" : 10},
    {"lvl":2,"displayedLevel":2,"pwr" : 3}
]

instead of doing it manually, we can use :
formateToContainer(input, {
    "lvl":"level",
    "displayedLevel":"level",
    "pwr":"power"
})
*/
function formateToContainer(input, relations) {
    var itemz = []
    for (var obj of input) {
        var item = {}
        for (var key in relations) {
            item[key] = obj[relations[key]];
        }
        itemz.push(item);
    }
    //console.log(JSON.stringify(itemz));
    return itemz;
}

function formateDate(date) {
    var d = new Date(date);
    var formatter = "/";
    var str = d.getDate() + formatter + (d.getMonth() + 1) + formatter + d.getFullYear();
    return str;
}
function formateDateForServer(y, m, d) {
    return y + "-" + m + "-" + d + "T" + "00:00:00";
}
function dateFormToDateTime(baseId) {
    return formateDateForServer(
        $("#" + baseId + "Year").val(),
        $("#" + baseId + "Month").val(),
        $("#" + baseId + "Day").val()
    );
}

function submitProject(data) {
    /*var data = {
        "Name":name,
        "Description":description,
        "RequiredProfile":requiredProfile,
        "StartingDate":startingDate,
        "Duration":duration,
        "DeadlineCV":CVDeadline
    }*/
    return ajaxPost("api/projectsApi", data)
        .done(function () {
            alert("project SENT !");
        }).fail(function () {
            alert("error");
        });
}

function applyCriteriasOnProjectList(projects, criterias) {
    var validProjects = [];

    // permet de savoir si l'élément key de proj respecte l'élément key de crit 
    // checks if proj respects crit  in terms of criteria "key"
    function isCritRespected(key, proj, crit) {
        return !(
            key in crit && // on check d'abord que les criteres contiennent la key sinon c'est forcément respecté
            Object.keys(crit[key]).length > 0 && // meme si la key existe, il faut qu'on ait au moins un élément
            !crit[key].includes(proj[key] + "")); // on fait le check
    }
    $.each(projects, function (k, v) {
        console.log(JSON.stringify(v.IdDepartment) + "test in :" + JSON.stringify(criterias.IdDepartment));
        if (!(isCritRespected("IdDepartment", v, criterias) &&
            isCritRespected("IdStatus", v, criterias) &&
            isCritRespected("IdLanguages", v, criterias) &&
            isCritRespected("IdBudget", v, criterias)
        )) {
            return true; // continue
        };
        if ("Name" in criterias && criterias["Name"] != "") {
            alert("testing");
            var regX = new RegExp(criterias["Name"]);
            if (!regX.test(v["Name"])) {
                return true;
            }
        }

        validProjects.push(v);
    });
    console.log(validProjects);
    return validProjects;
}

function findCommonDataById(dataName, id) {
    var rel = {
        departments: {
            arrayName: "departments",
            idName: "IdDepartment"
        },
        skills: {
            arrayName: "skills",
            idName: "IdSkills"
        },
        languages: {
            arrayName: "languages",
            idName: "IdLanguage"
        },
        areas: {
            arrayName: "areas",
            IdName: "IdArea"
        },
        status: {
            arrayName: "status",
            IdName: "IdStatus"
        }
    }
    if (!dataName in rel) {
        console.log("Error dataName (" + dataName + ") not found in :" + JSON.stringify(rel));
    }
    var scope = rel[dataName];
    for (var el of commonData[scope.arrayName]) {
        if (el.scope.IdName == id) {
            return el;
        }
    }
    console.log("findCommonDataById : (" + id + ") not found. Data :" + JSON.stringify(commonData[scope.arrayName]));
    return {};
}
/*
turns [
    [1,2,3]
]
 with keys ["num1","num2","num3"]
 to :
 [
     {
         num1 : 1,
         num2 : 2,
         num3 : 3
     }
 ]

*/
function arrayNumIndexToStrIndex(keys, array) {
    var nArray = [];
    for (var entry in array) {
        nArray[entry] = {};
        for (var key in array[entry]) {
            nArray[entry][keys[key]] = array[entry][key];
        }
    }
    return nArray;
}
function cleanCommonData() {

    // simple clean

    /* turning complex commonData into something easier to use like :
        id : name,
        id : name
        (-> numeric array)
    */
    // console.log(commonData);

    /* other benefits :
         we only keep relevant data
         id inside of the numeric array is actually relevant, we don't want something like :
         area : [
             {
                 idArea : 5
             }
         ]
         (area[0] would actually contain the area wwith ID 5)
    */
    /*
      var toBeCleaned = arrayNumIndexToStrIndex(
          ["objectName","idName","name"],[
          ["languages","IdLanguage","Name"],
          ["departments","IdDepartment","Name"],
          ["skills","IdSkills","SkillsName"],
          ["budgets","IdBudget","Name"],
          ["countries","IdCountry","Name"],
          ["status","IdStatus","Name"],
          ["genders","IdGender","Name"]
  
      ]);*/
    // console.log(toBeCleaned);
    /*
    for(var cleaning of toBeCleaned){
        var tmp = commonData[cleaning.objectName];
        //console.log(commonData[cleaning.objectName]);
        delete commonData[cleaning.objectName];
        commonData[cleaning.objectName] = [];
        //console.log(tmp);
        for(var entry of tmp){
            commonData[cleaning.objectName][entry[cleaning.idName]] = entry[cleaning.name];
        }
    }*/
    // complex clean
    /*
        the "translation" work is harder here, instead of going for id:name , we have to get some data as well
        so we are going to retrieve it in that way :
        before :

        objectName : [
            {
                idName : x,
                data.Name : name
                data.otherdata : data
            }
        ]

        after :

        objectName : [
            id : {
                name : xxx
                data : xxx
            }
        ]

    */
    var toBeCleaned = [
        {
            objectName: "areas",
            idName: "IdArea",
            data: {
                "Name": "name",
                "IdCountry": "idCountry"
            }
        },
        {
            objectName: "languages",
            idName: "IdLanguage",
            data: {
                "Name": "name"
            }
        },
        {
            objectName: "departments",
            idName: "IdDepartment",
            data: {
                "Name": "name"
            }
        },
        {
            objectName: "skills",
            idName: "IdSkills",
            data: {
                "SkillsName": "name"
            }
        },
        {
            objectName: "budgets",
            idName: "IdBudget",
            data: {
                "Name": "name",
                "RealName" : "gname"
            }
        },
        {
            objectName: "countries",
            idName: "IdCountry",
            data: {
                "Name": "name"
            }
        },
        {
            objectName: "status",
            idName: "IdStatus",
            data: {
                "Name": "name"
            }
        },
        {
            objectName: "genders",
            idName: "IdGender",
            data: {
                "Name": "name"
            }
        },
        {
            objectName: "languagesLevels",
            idName: "IdLevel",
            data: {
                "Name": "name"
            }
        },
        {
            objectName: "degreesTypes",
            idName: "IdDegreesType",
            data: {
                "Name": "name"
            }
        },

    ]
    for (var cleaning of toBeCleaned) {
        var tmp = commonData[cleaning.objectName];
        delete commonData[cleaning.objectName];
        commonData[cleaning.objectName] = {};
        for (var entry of tmp) {
            commonData[cleaning.objectName][entry[cleaning.idName]] = {};
            $.each(cleaning.data, function (k, v) {
                commonData[cleaning.objectName][entry[cleaning.idName]][v] = entry[k];
            });
        }
    }
    
    // company
    var tmp = commonData.company;
    delete commonData.company;

    commonData.company = {};
    $.each(tmp,function(k,v){
        commonData.company[k] = {name : v.Company}
    });

    //console.log(commonData);
}

// injects the selected="selected" into a select data array from the array contining the IDs that are selected
function injectSelectedDataIntoJson(id, select) {
    //console.log(id);
    //console.log(select);
    if (Array.isArray(id)) {
        for (var oneId of id) {
            injectSelectedDataIntoJson(oneId, select);
        }
    } else {
        for (var k in select.options) {
            if (select.options[k].value == id) {
                select.options[k].selected = true;
                return;
            }
        }
    }
}
// to convert a dtepicker to a datetime item
function datepickerToDateTime(id) {
    var date = new GDate();
    date.createDateFromFormData(id);
    console.log(date);
    return date.getDateTime();
}
/*
    [
        {id:label},
    ]
    ->
    {
        $id : {
            id : $id
            label : $label
        },
    }
*/
function getBasicFormData(data) {
    var res = {};
    $.each(data, function (k, v) {
        res[k] = {}
        res[k]["id"] = k;
        res[k]["label"] = v;
    });
    return res;
}
function isObject(obj) {
    return obj === Object(obj);
}

// automatic import of form data
// must give obj as a container for data (gotta be an object of the relevant data type)
// must give data to bind forms
// simple string is good if we want to link a simple text form to an object key with the same name
// otherwise we have to use an object instead of a string, we will have to give it properties like :
// the key in the object, the id in the form, the type of binding, and maybe different stufes in the future
function importFormData(obj, data) {
    $.each(data, function (k, v) {
        if (!isObject(v)) {
            obj[v] = $("#" + v).val();
        } else if ("type" in v) {
            switch (v.type) {
                case "rel":
                    obj[v.key] = $("#" + v.id);
                    return;
                case "date":
                    obj[v.key] = new GDate(v.id);
                    return;
            }
        }
    });
}

function activateLanguageSelection() {

    function activateDeleteButtons() {

        $(".languageRowDelete").off().on('click', function () {
            $(this).closest(".languageRow").remove();
        });
    }
    function triggerCheckForDifferentValues() {

        $(".langSelecter").off().on("change", function () {
            checkForDifferentValues();
        })
    }
    function checkForDifferentValues() {
        var ids = {};
        $(".langSelecter").each(function () {
            var val = $(this).val();
            if (!val) {
                return true;
            }
            if (!ids[val]) {
                ids[val] = true;
            } else {
                console.log("adding :", val, ids);
                $(this).val(null);
                alert("Error, you already added this language.");
            }

        });
    }
    $("#langAddRow").click(function () {
        $("#languageSelectionContainer").append(getLanguageRow());
        activateDeleteButtons();
        triggerCheckForDifferentValues();
    });
    triggerCheckForDifferentValues();
    activateDeleteButtons();

}
function getLanguageRow() {
    return $.render.inputLanguageRow({
        langOptions: DropdownListData.generateOptionListJson(commonData.languages),
        lvlOptions: DropdownListData.generateOptionListJson(commonData.languagesLevels)
    });
}
function getRadioResult(name) {
    var result;
    $("[name=" + name + "]").each(function () {
        if ($(this).prop('checked')) {
            result = $(this).val();
            return false;
        }
    });
    return result;
}
function getVal(id) {
    return $("#" + id).val();
}
function getVals(obj, ids) {
    for (var id of ids) {
        obj[id] = getVal(id);
    }
    return obj;
}
function fixIds(tplData, id) {
    $.each(tplData, function (k, v) {
        tplData[k]["id"] = tplData[k]["id"] + "" + id;
    });
    return tplData;
}
function elExist(id) {
    var elId = "#" + id;
    if ($(elId).length) {
        //console.log("Element ", elId ," found");
        return true;
    }
    //console.log("Element ", elId ," NOT found");
    return false;

}

function autoBulletPointsTextarea(id) {
    // https://stackoverflow.com/questions/19383962/how-to-add-each-time-i-hit-enter-key-in-textarea
    var linestart = function (txt, st) {
        var ls = txt.split("\n");
        var i = ls.length - 1;
        ls[i] = st + ls[i];
        return ls.join("\n");
    };
    $('#' + id).on('keydown', function (e) {
        var t = $(this);
        if (e.which == 13) {
            // ☆ ► • → ○ ♦ ◘ ♥ ☼
            t.val(linestart(t.val(), '• ') + "\n");
            return false;
        }
    });
}

function regionPicker(countryId,regionId){
    function performCheck(){
        var id = getVal(countryId);
        var optionList = []
        $.each(commonData["areas"],function(k,v){
            if(v.idCountry == id){
                optionList.push({
                    option:v.name,
                    value:k
                });
            }
        });
        var html = "";
        for(var option of optionList){
            html += $.render.optionList(option);
        }
        $("#"+regionId).html(html);
        if(html != ""){
            $("#"+regionId).closest(".row").slideDown(200);
        } else {
            $("#"+regionId).closest(".row").slideUp(200);
        }
    }
    $("#"+countryId).on("change",function(){
        performCheck();
    });
    performCheck();
    

}
function clone(object){
    return JSON.parse(JSON.stringify(object));
}
function mt_rand(min,max){
    return Math.floor(Math.random()*(max-min+1)+min);
}
function toObject(arr) {
    var rv = {};
    for (var i = 0; i < arr.length; ++i)
      rv[i] = arr[i];
    return rv;
}
function mkButton(id,label,light){
    return light ? {id : id , label : label , class : "genyzLightButton"}
                 : {id : id , label : label};
}
function mkSimple(id,label){
    return mkButton(id,label);
}
function getDefaultAvatar(sex){
    return (!sex || commonData.genders[sex] != "Female") ? "defaultAvatarM"
                                                         : "defaultAvatarF";
}