
var lang = {
    "langList": ["en", "fr"],
    "en": {
        "login": "Login",
        "mailAddress": "Mail address",
        "enterMail": "Enter email",
        "password": "Password",
        "enterPassword": "Enter password",
        "passwordConfirmation": "Password confirmation",
        "submit": "Submit",

        "register": "Register",
        "firstname": "Firstname",
        "enterFirstname": "Enter firstname",
        "lastname": "Lastname",
        "enterLastname": "Enter lastname",
        "birthday": "Birthday",
        "streetNumberAndName": "Street number and name",
        "streetNumber": "Street number",
        "streetName": "Street name",
        "postcodeAndCity": "Postcode and city name",
        "postcode": "Postcode",
        "city": "City name",
        "selectYourCountry": "Select your country",
        "country": "Country",
        "nationalRegisterNumber": "National register number",
        "phoneNumber": "Phone number",
        "secondPhoneNumber": "Secondary phone number",
        "optional": "Optional",


        "viewUserList": "View user list",
        "viewProjectList": "View project list",
        "reviewBookmarksAndApplications": "Review bookmarks and applications",
        "manageDepartmentsAndCompetenciesSuggestions": "Manage departments and competencies suggestions",
        "generateSubscriptionLink": "Generate subscription link",

        "editMyCvAndProfile": "Edit my CV and profile",
        "lookForProjects": "Look for projects",

        "editMyProfile": "Edit my profile",
        "manageMyProjects": "Manage my projects",
        "viewConsultantList": "View consultants list",

        "viewConsultantAndCandidateList": "View consultant and candidate list",


        "addNewProject": "Add a new project",
        "changeCriterias": "Change criterias",

        "projectCreation": "Project creation",
        "projectName": "Project name",

        "projectDuration": "Project duration",
        "months": "Months",
        "projectStartingDate": "Project starting date",
        "competencies": "Competencies",
        "languages": "Languages",
        "region": "Region",
        "budget": "Budget",
        "applicationDeadline": "Application deadline",
        "description": "Project description",
        "profileNeeded": "Profile needed",
        "saveDraft": "Save draft",
        "submitToValidation": "Submit to validation",


        "projectCriteriasSelection": "Project criterias selection",
        "projectStatus": "Project status",
        "sectors": "Sectors",
        "projectStarts": "Project starts",
        "projectEnds": "Project ends",
        "search": "Search",

        "pleaseTellUsWhyYouWantToDeleteYourProject": "Please tell us why you want to delete your project",
        "additionalComments": "Additional comments",



        "interests": "Interests",
        "profileEdition": "Profile edition",
        "sex": "Sex",
        "sexMale": "Male",
        "sexFemale": "Female",
        "sexOther": "Other",
        "function": "Function",


        /*
        "countries": {
            "be": "Belgium",
            "fr": "France",
            "nl": "Netherlands",
            "de": "Deutchland",
            "lu": "Luxembourg"
        }*/
        "departments": "Departments",
        "sectorInterests": "Sector interests",
        "competenciesInterests": "Competencies interests",

        "next": "Next",
        "back": "Back"
    },
    "fr": {
        "login": "Connexion",
        "mailAddress": "Addresse mail",
        "enterMail": "Entrez votre addresse mail",
        "password": "Mot de passe",
        "enterPassword": "Entrez votre mot de passe",
        "passwordConfirmation": "Confirmation du mot de passe",
        "submit": "Envoyer",

        "register": "Inscription",
        "firstname": "Prénom",
        "enterFirstname": "Entrez votre prénom",
        "lastname": "Nom",
        "enterLastname": "Entrez votre nom",
        "birthday": "Date de naissance",
        "streetNumberAndName": "Numéro et nom de rue",
        "streetNumber": "Numéro de rue",
        "streetName": "Nom de rue",
        "postcodeAndCity": "Code postal et nom de la ville",
        "postcode": "Code postal",
        "city": "Ville",
        "selectYourCountry": "Choisissez votre pays",
        "country": "Pays",
        "nationalRegisterNumber": "Numéro de registre national",
        "phoneNumber": "Numéro de téléphone",
        "secondPhoneNumber": "Numéro de téléphone secondaire",
        "optional": "Optionnel",


        "viewUserList": "Voir la liste des utilisateurs",
        "viewProjectList": "Voir la liste des projets",
        "reviewBookmarksAndApplications": "Consulter les bookmarks et postulations",
        "manageDepartmentsAndCompetenciesSuggestions": "Gérer les suggestions d'ajout dedépartements et compétences",
        "generateSubscriptionLink": "Générer un lien d'inscription",

        "editMyCvAndProfile": "Modifier mon CV / profil",
        "lookForProjects": "Rechercher des projets",

        "editMyProfile": "Modifier mon profil",
        "manageMyProjects": "Gérer mes projets",
        "viewConsultantList": "Voir la liste des consultants",

        "viewConsultantAndCandidateList": "Voir la liste des consultants et des candidats",

        "addNewProject": "Ajouter un nouveau projet",
        "changeCriterias": "Modifier les critères ",

        "projectCreation": "Création de projet",
        "projectName": "Nom du projet",

        "projectDuration": "Durée du projet",
        "months": "Mois",
        "projectStartingDate": "Date de début du projet",
        "competencies": "Compétences",
        "languages": "Langues",
        "region": "Région",
        "budget": "Budget",
        "applicationDeadline": "Cloture des postulations",
        "projectDescription": "Description du projet",
        "profileNeeded": "Profil requis",
        "saveDraft": "Sauvegarder le brouillon",
        "submitToValidation": "Envoyer à la validation",


        "projectCriteriasSelection": "Selection des critères de projet",
        "projectStatus": "Statut du projet",
        "sectors": "Secteurs",
        "projectStarts": "Début du projet",
        "projectEnds": "Fin du projet",
        "search": "Chercher",

        "pleaseTellUsWhyYouWantToDeleteYourProject": "Veuillez nous dire pourquoi vous voulez supprimer votre projet",
        "additionalComments": "Commentaires supplémentaires"


    }
}
var sessionData = {
    "userLevel": 0, // 0-> not connected, 1-> admin 2-> customers 3-> consultant 4-> candidat
    "username": "",
    "mail": "",
    "lang": "en",
    "location": []
}

var user;

var commonData = {
    roles : {
        1 : { name : "Admin"},
        2 : { name : "Customers"},
        3 : { name : "Consultants"},
        4 : { name : "Candidates"},
        5 : { name : "NewCandidates"}
    }
}

var userCVS = []







var defaultPage = "#login";


var tplList = {



    "dashboard": "templates/dashboard/dashboard.tpl.html",
    "dashboardItem": "templates/dashboard/dashboardItem.tpl.html",


    "inputAddress": "templates/form/inputAddress.tpl.html",
    "inputMultiple": "templates/form/inputMultiple.tpl.html",
    "inputText": "templates/form/inputText.tpl.html",
    "multiFieldText": "templates/form/multiFieldText.tpl.html",
    "multipleLanguagesWithLevelSelect": "templates/form/multipleLanguagesWithLevelSelect.tpl.html",
    "multipleLanguagesWithLevelSelectRow": "templates/form/multipleLanguagesWithLevelSelectRow.tpl.html",
    "radio": "templates/form/radio.tpl.html",
    "checkbox": "templates/form/checkbox.tpl.html",
    "select": "templates/form/select.tpl.html",
    "selectDate": "templates/form/selectDate.tpl.html",
    "selectSidedDate": "templates/form/selectSidedDate.tpl.html",
    "selectMultiple": "templates/form/selectMultiple.tpl.html",
    "sliderWithInputText": "templates/form/sliderWithInputText.tpl.html",
    "submit": "templates/form/submit.tpl.html",
    "textarea": "templates/form/textarea.tpl.html",
    "optionList": "templates/form/optionList.tpl.html",
    "inputLanguage": "templates/form/inputLanguage.tpl.html",
    "inputLanguageRow": "templates/form/inputLanguageRow.tpl.html",
    "availability": "templates/form/availability.tpl.html",
    "region": "templates/form/region.tpl.html",
    "addLink": "templates/form/addLink.tpl.html",

    "projectCriteria": "templates/projects/projectCriteria.tpl.html",
    "projectList": "templates/projects/projectList.tpl.html",
    "projectListItem": "templates/projects/projectListItem.tpl.html",
    "projectAdd": "templates/projects/projectAdd.tpl.html",
    "projectView": "templates/projects/projectView.tpl.html",
    "projectDeletion": "templates/projects/projectDeletion.tpl.html",
    "projectCriteriaSelection": "templates/projects/projectCriteriaSelection.tpl.html",

    "buttonGroup": "templates/misc/buttonGroup.tpl.html",

    "profileEdition": "templates/profileEdition/profileEdition.tpl.html",
    "editProfile": "templates/profileEdition/editProfile.tpl.html",
    "selectCV": "templates/profileEdition/selectCV.tpl.html",
    "editContactData": "templates/profileEdition/editContactData.tpl.html",
    "editConnectionData": "templates/profileEdition/editConnectionData.tpl.html",
/*
    "CVEdition": "templates/profileEdition/editCV/CVEdition.tpl.html",
    "editCV": "templates/profileEdition/editCV/editCV.tpl.html",
    "addEducation": "templates/profileEdition/editCV/addEducation.tpl.html",
    "addExperience": "templates/profileEdition/editCV/addExperience.tpl.html",
    "addTraining": "templates/profileEdition/editCV/addTraining.tpl.html",
*/
    "CVElementItemLine": "templates/profileEdition/editCV/CVElementItemLine.tpl.html",

    "CVGeneral": "templates/profileEdition/editCV/CVGeneral.tpl.html",

    "CVEducation": "templates/profileEdition/editCV/education/CVEducation.tpl.html",
    "CVEducationItem": "templates/profileEdition/editCV/education/CVEducationItem.tpl.html",

    "CVTraining": "templates/profileEdition/editCV/training/CVTraining.tpl.html",
    "CVTrainingItem": "templates/profileEdition/editCV/training/CVTrainingItem.tpl.html",

    "CVExperience": "templates/profileEdition/editCV/experience/CVExperience.tpl.html",
    "CVExperienceItem": "templates/profileEdition/editCV/experience/CVExperienceItem.tpl.html",

    "CVElementCrudIcons" : "templates/profileEdition/editCV/CVElementCrudIcons.tpl.html",



    userList : "templates/userList/userList.tpl.html",
    userListItem : "templates/userList/userListItem.tpl.html",
    userSort : "templates/userList/userSort.tpl.html",
    userCriteria : "templates/userList/userCriteria.tpl.html",

    userTypePicker : "templates/userList/userTypePicker.tpl.html",

    "login": "templates/login.tpl.html",
    "register": "templates/register.tpl.html"
}

var apiDataRetrievement = {
    areas : "api/AreasAPI",
    departments : "api/DepartmentsAPI",
    degreesTypes : "api/DegreesTypesAPI",
    skills : "api/SkillsAPI",
    budgets : "api/BudgetsAPI",
    //educations : "api/EducationsAPI",
    languages : "api/LanguagesAPI",
    languagesLevels : "api/LevelsAPI",
    //degrees : "api/DegreesAPI",
    status : "api/StatusAPI",
    //experiences : "api/ExperiencesAPI",
    contacts : "api/ContactsAPI",
    countries : "api/CountriesAPI",
    genders : "api/GendersAPI",
    company : "api/CustomersAPI"
    
};

var apiRoot = "http://localhost:64265/";

var currentCustomerProjects = [];
var currentProjectCriterias = {};