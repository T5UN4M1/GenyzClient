
function getDefaultTemplate(tplName) {
    switch (tplName) {
        case "address":
            return {
                streetNumberAndName: {
                    firstId: "sreetNumber",
                    label: "Street name and number",
                    input: [
                        { mobileWidth: 9, desktopWidth: 8, id: "streetName", placeholder: getLang().streetName },
                        { mobileWidth: 3, desktopWidth: 4, id: "streetNumber", placeholder: "Number" }
                        
                    ]
                },
                postcodeAndCity: {
                    firstId: "postcode",
                    label: "Postcode and city",
                    input: [
                        { mobileWidth: 3, desktopWidth: 4, id: "postcode", placeholder: "PC" },
                        { mobileWidth: 9, desktopWidth: 8, id: "city", placeholder: "City" }
                    ]
                }
            };

        case "projectName":
            return {
                id: "projectName",
                label: "Project name"
            };
        case "projectDuration":
            return {
                id: "projectDuration",
                label: "Project duration",
                valLabel: "months"
            }
        case "projectStatus":
            var pStatus = {
                id: "projectStatus",
                label: "Status",
                options: []
            };
            pStatus.options = DropdownListData.generateOptionListJson(commonData.status);
            return pStatus;
        case "projectDepartments":
            var pDepartment = {
                id: "projectDepartment",
                label: "Department",
                options: []
            }
            pDepartment.options = DropdownListData.generateOptionListJson(commonData.departments);
            return pDepartment;
        case "projectSkills":
            var pSkills = {
                id: "projectSkills",
                label: "Which competencies are required for this project ?",
                options: []
            }
            pSkills.options = DropdownListData.generateOptionListJson(commonData.skills);
            return pSkills;
        case "projectBudgets":
            var pBudget = {
                id: "projectBudget",
                label: "Budget",
                options: []
            }
            pBudget.options = DropdownListData.generateOptionListJson(commonData.budgets);
            return pBudget;
        case "projectStartDate":
            return {
                id: "projectStartDate",
                label: "Project start date"
            };
        case "projectEnds":
            return {
                id: "projectEnds",
                label: "End"
            };
        case "projectCVDeadline":
            return {
                id: "projectCVDeadline",
                label: "Deadline for applications"
            }
        case "projectDescription":
            return {
                id: "projectDescription",
                label: "Project description",
                rows: 5
            };
        case "projectRequiredProfile":
            return {
                id: "projectProfileNeeded",
                label: "Summary of the profile required",
                rows: 5
            };
        case "projectAreas":
            var pAreas = {
                id: "projectArea",
                label: "Location",
                options: []
            }
            pAreas.options = DropdownListData.generateOptionListJson(commonData.areas);
            return pAreas;
        case "projectLanguages" : 
            var langTplData = {
                label : "Which languages are required for this project ?",
                languages : [
                    {
                        langOptions : DropdownListData.generateOptionListJson(commonData.languages),
                        lvlOptions : DropdownListData.generateOptionListJson(commonData.languagesLevels)
                    }
                ],
                
                langAddRow: {
                    id:"langAddRow",
                    label:"Add language"
                }
                
            }
            return langTplData;
        case 'firstName': 
            return{
                id: "firstName",
                label: "First name"
            }
        case 'lastName': 
            return {
                id: "lastName",
                label: "Last name"
            }
        case 'birth': 
            return {
                id: "birth",
                label: "Date of birth"
            }
        case 'nationalRegisterNumber': 
            return {
                id: "nationalRegisterNumber",
                label: "National Register number"
            }
        case 'sex': 
            return{
                id: "sex",
                label: "Gender",
                options: DropdownListData.generateOptionListJson(commonData.genders)
            }
        case 'mail': 
            return {
                id: "mail",
                label: "New email Address"
            }
        case 'password': 
            return {
                id: "password",
                label: "Password",
                type: "password"
            }
        case 'password1' :
            return {
                id: "password1",
                label: "New password",
                type: "password"
            }
        case 'password2': 
            return {
                id: "password2",
                label: "New password (confirmation)",
                type: "password"
            }
        case 'oldPassword': 
            return {
                id: "oldPassword",
                label: "Old password",
                type: "password"
            }
        case 'phone1': 
            return{
                id: "phone1",
                label: "Phone number"
            }
        case 'phone2': 
            return {
                id: "phone2",
                label: "Secondary phone number"
            }
        case 'country': return {
                id: "country",
                label: "Country",
                options: DropdownListData.generateOptionListJson(commonData.countries)
            }
        case 'availability': 
            var availabilityTplData =  {
                label : "When would you be available?",
                type1 : {},
                type2 : {},
                type3 : {}
            }
            for(let i=1;i<4;++i){
                availabilityTplData["type"+i]["id"] = "availability" + i;
                availabilityTplData["type"+i]["name"] = "availability";
                availabilityTplData["type"+i]["value"] = i;
            }
            availabilityTplData["type1"]["label"] = "Immediately";
            availabilityTplData["type2"]["availabilityUnder"] = {
                id : "availabilityUnder",
                alone : true
            };
            availabilityTplData["type3"]["availabilityAsFrom"] = {
                id : "availabilityAsFrom",
                alone : true
            };
            return availabilityTplData;
        case "location":
            var locationData = {
                label : "Where would you like to work ?",
                countryId : "country",
                regionId : "region",
                countryList : []
            }
            locationData.countryList = DropdownListData.generateOptionListJson(commonData.countries);
            return locationData;
        case "jobTitle" : return {
            label : "Current/most recent job title",
            id : "jobTitle"
        }
        case "departmentInterest" :             
            var departments = {
                id: "departmentInterest",
                label: "Which department(s) are you interested in?",
                options: []
            }
            departments.options = DropdownListData.generateOptionListJson(commonData.departments);
            return departments;
        case "skillInterest" : 
            var skills = {
                id: "skillInterest",
                label: "Which competencies would you like to develop?",
                options: []
            }
            skills.options = DropdownListData.generateOptionListJson(commonData.skills);
            return skills;
        case "CVTitle" :
            return {
                id: "Title",
                label : 'CV title <br /><span class="hint">This title will appear on your CV</span>',
                placeholder : 'CV title'
            }
        case "CVDepartment" :
            var data =  {
                id: "department",
                label : "Areas of expertise",
                options : []
            }
            data.options = DropdownListData.generateOptionListJson(commonData.departments);
            return data;
        case "CVSeniority":
            var data = {
                id: "seniority",
                label : "Seniority",
                options : []
            }
            data.options = DropdownListData.generateOptionListJson(commonData.budgets);
            return data;
        case "CVExperience" : 
            var data = {
                id: "startDate",
                label : "Years of experience",
                addedText : "I have been working since",
                alone: true,
                placeholder : "yyyy"
            }
            return data;
        case "CVSkill": 
            var data = {
                id: "skill",
                label : "Competencies",
                options : []
            }
            data.options = DropdownListData.generateOptionListJson(commonData.skills);
            return data;
        case "CVLanguage":
            var data = {
                labelClass : "whiteTitle",
                label : "Languages",
                buttonOffset : 8,
                languages : [
                    {
                        langOptions : DropdownListData.generateOptionListJson(commonData.languages),
                        lvlOptions : DropdownListData.generateOptionListJson(commonData.languagesLevels)
                    }
                ],
                
                langAddRow: {
                    id:"langAddRow",
                    label:"Add language"
                }
                
            }
            return data;




        /////////////////////////////
        /// EDUCATIONS
        /////////////////////////////
        
        // SINGLE
        case "CVDegree":
            var data =  {
                id: "highestDegree",
                label : "Highest degree(s) obtained",
                maxOptions : 2,
                options : []
            }
            data.options = DropdownListData.generateOptionListJson(commonData.degreesTypes);
            return data;

        // Add
        case "CVAddEducation":
            return {
                id:"addEducationForm",
                label:"Add another degree"
            }

        // REPEAT
        case "CVEducationDegreeName" : 
            return {
                id:"name",
                label:"Degree name"
            }
        case "CVEducationSchoolName":
            return {
                id:"schoolName",
                label:"School name"
            }
        case "CVEducationDuration" :
            return {
                id:"duration",
                label:"Duration",
                placeholder:"Duration , E.G : 3 years"
            }
        case "CVEducationEndDate" :
            return {
                id:"endDate",
                label:"Completion date",
                fullLength : true
            }

        case "CVEducation" :
            var list = ["CVEducationDegreeName","CVEducationSchoolName","CVEducationDuration","CVEducationEndDate"]
            var data = {}
            for(let r of list){
                data[r] = getDefaultTemplate(r);
            }
            return data;




        ////////////////////////////
        // TRAININGS
        ///////////////////////////

        // SINGLE

        // ADD
        case "CVAddTraining":
            return {
                id:"addTrainingForm",
                label:"Add another training"
            }

        // REPEAT

        case "CVTrainingTrainingName" : 
            return {
                id:"name",
                label:"Training name"
            }
        case "CVTrainingInstitutionName":
            return {
                id:"institutionName",
                label:"Institution name"
            }
        case "CVTrainingDuration" :
            return {
                id:"duration",
                label:"Duration",
                placeholder:"Duration , E.G : 6 months"
            }
        case "CVTrainingEndDate" :
            return {
                id:"endDate",
                label:"Completion date",
                fullLength : true
            }
        case "CVTraining" :
            var list = ["CVTrainingTrainingName","CVTrainingInstitutionName","CVTrainingDuration","CVTrainingEndDate"]
            var data = {}
            for(let r of list){
                data[r] = getDefaultTemplate(r);
            }
            return data;

        ////////////////////////////////////
        //// EXPERIENCE
        ////////////////////////////////////

        // SINGLE
        case "CVExperienceSummary" : 
            return {
                id : "summary",
                label : "Experience summary",
                placeholder :"Please use bullet points",
                rows : 5
            }
        
        // ADD

        case "CVAddExperience":
            return {
                id:"addExperienceForm",
                label:"Add another experience"
            }


        // REPEAT

        case "CVExperienceJobTitle" : 
            return {
                id:"jobTitle",
                label:"Job title"
            }
        case "CVExperienceCompany" : 
            return {
                id:"company",
                label:"Company"
            }
        case "CVExperienceLocation" : 
            return {
                id:"location",
                label:"Location"
            }
        case "CVExperienceStartDate" : 
            return {
                id:"startDate",
                label:"Start date",
                fullLength : true
            }
        case "CVExperienceEndDate" : 
            return {
                id:"endDate",
                label:"End date",
                fullLength : true
            }
        case "CVExperienceProjectDescription" : 
            return {
                id:"description",
                label:"Project description",
                placeholder: "Please use bullet points",
                rows : 4,
                pre : true
            }
        case "CVExperienceFields" : // warning , had to add "Fields" to solver case conflict, there is a CVExperience above /!\
            var list = ["CVExperienceJobTitle","CVExperienceCompany","CVExperienceLocation",
            "CVExperienceStartDate", "CVExperienceEndDate","CVExperienceProjectDescription"];
            var data = {}
            for(let r of list){
                data[r] = getDefaultTemplate(r);
            }
            return data;
        



        case "company" : 
            return {
                id : "company",
                label : "Company"
            }
        
    }
}
function tpl(str){
    return getDefaultTemplate(str);
}