var profileEditionForm = {
    "formFirstname": {
        "tpl": "inputTextSimple",
        "label": getLang().firstname
    },
    "formLastname": {
        "tpl": "inputTextSimple",
        "label": getLang().lastname
    },
    "formMail": {
        "tpl": "inputTextSimple",
        "label": getLang().mailAddress
    },
    "formSex": {
        "tpl": "selectSimple",
        "label": getLang().sex,
        "options": [
            { "value": "m", "option": getLang().sexMale },
            { "value": "f", "option": getLang().sexFemale },
            { "value": "o", "option": getLang().sexOther }
        ]
    },
    "formNationalRegisterNumber": {
        "tpl": "inputTextSimple",
        "label": getLang().nationalRegisterNumber
    },
    "formLanguagesSelect": {
        "tpl": "selectMultiple",
        "id": "languagesSelect",
        "label": getLang().languages,
        "options": [
            { "value": "fr", "option": "French" },
            { "value": "en", "option": "English" }
        ]
    },
    "formSubmit": {
        "tpl": "submitSimple",
        "id": "profileEditionSubmit",
        "label": getLang().submit
    }
};
var connectionData = {

    "formMail": {
        "tpl": "inputTextSimple",
        "label": getLang().mailAddress
    },
    "formPassword": {
        "tpl": "inputTextSimple",
        "id": "pwd1",
        "type": "password",
        "label": getLang().password
    },
    "formPassword1": {
        "tpl": "inputTextSimple",
        "id": "pwd1",
        "type": "password",
        "label": getLang().password
    },
    "formPassword2": {
        "tpl": "inputTextSimple",
        "id": "pwd1",
        "type": "password",
        "label": getLang().password
    },
    "formPasswordOld": {
        "tpl": "inputTextSimple",
        "id": "pwd1",
        "type": "password",
        "label": getLang().password
    },
    "formSubmit": {
        "tpl": "submitSimple",
        "id": "profileEditionSubmit",
        "label": getLang().submit
    }
}
var contactDataForm = {
    "formPhone1": {
        "tpl": "inputTextSimple",
        "label": getLang().phoneNumber
    },
    "formPhone2": {
        "tpl": "inputTextSimple",
        "label": getLang().secondPhoneNumber
    },
    "formStreetNumberAndName": {
        "tpl": "inputMultiple",
        "firstId": "streetName",
        "label": getLang().streetNumberAndName,
        "input": [
            { "mobileWidth": 3, "desktopWidth": 3, "id": "streetNumber", "placeholder": getLang().streetNumber },
            { "mobileWidth": 9, "desktopWidth": 9, "id": "streetName", "placeholder": getLang().streetName }
        ]
    },
    "formPostcodeAndCity": {
        "tpl": "inputMultiple",
        "firstId": "postcode",
        "label": getLang().streetNumberAndName,
        "input": [
            { "mobileWidth": 3, "desktopWidth": 3, "id": "postcode", "placeholder": getLang().postcode },
            { "mobileWidth": 9, "desktopWidth": 9, "id": "city", "placeholder": getLang().city }
        ]
    },

    "formCountrySelect": {
        "tpl": "selectSimple",
        "label": getLang().country,
        "options": [
            { "value": "be", "option": "Belgium" },
            { "value": "fr", "option": "France" }
        ]
    },
    "formSubmit": {
        "tpl": "submitSimple",
        "id": "registerSubmit",
        "label": getLang().submit
    },
}
var workDataForm = {
    "formFunction": {
        "tpl": "inputTextSimple",
        "label": getLang().function
    },
    "formDisponibility": {
        "tpl": "radioSimple",
        "options": [
            { "name": "disponibility", "id": "immediately", "label": "immediately" },
            { "name": "disponibility", "id": "under", "label": "under" },
            { "name": "disponibility", "id": "asFrom", "label": "as from" }
        ]
    },
    "formUnder": {
        "tpl": "inputTextSimple",
        "id": "disponibilityUnder"
    },
    "formAsFrom": {
        "tpl": "inputMultiple",
        "firstId": "asFromDay",
        "label": "as from",
        "input": [
            { "mobileWidth": 1, "desktopWidth": 1, "id": "asFromDay", "placeholder": getLang().day },
            { "mobileWidth": 1, "desktopWidth": 1, "id": "asFromWeek", "placeholder": getLang().week },
            { "mobileWidth": 2, "desktopWidth": 2, "id": "asFromYear", "placeholder": getLang().year}
        ]
    },
    "formSectorsSelect": {
        "tpl": "selectMultiple",
        "id": "sectorSelect",
        "label": getLang().departments,
        "options": [
            { "value": "sector1", "option": "1" },
            { "value": "sector2", "option": "2" }
        ]
    },
    "formSectorsInterestsSelect": {
        "tpl": "selectMultiple",
        "id": "sectorInterestsSelect",
        "label": getLang().sectorInterests,
        "options": [
            { "value": "sector1", "option": "1" },
            { "value": "sector2", "option": "2" }
        ]
    },
    "formCompetenciesInterestsSelect": {
        "tpl": "selectMultiple",
        "id": "competenciesInterestsSelect",
        "label": getLang().competenciesInterests,
        "options": [
            { "value": "comp", "option": "comp" },
            { "value": "comp", "option": "comp" }
        ]
    }
}

$(function () {

    var loadedTplCount=0,tplAmount = Object.keys(tplList).length,
    apiDataCount=0,apiDataAmount = Object.keys(apiDataRetrievement).length,
    loadedCount=0,loadAmount=2;

    function moduleLoaded(){
        ++loadedCount;
        if(loadedCount == loadAmount){
            router();
        }
    }
    $.each(tplList, function(key,val){
        $.ajax({
            url:val,
            success: function(data){
                $.templates(key,data);
                console.log("Template '"+key+"' loaded ("+(loadedTplCount+1)+"/"+tplAmount+")");
                ++loadedTplCount;
                if(loadedTplCount==tplAmount){
                    moduleLoaded();
                }
            },
        });
    }); 
    /*
        countries IDs must remain as it 
    */
    $.each(apiDataRetrievement, function(key,val){
        ajaxGet(val).done(function(data){
            //console.log(data);
            commonData[key] = data;
            console.log("Api data '"+ key +"' loaded ("+(apiDataCount+1)+"/"+apiDataAmount+")");
            ++apiDataCount;
            if(apiDataCount == apiDataAmount){
                cleanCommonData();
                console.log("Common data :",commonData);
                moduleLoaded();
                
            }
        })
    })

    function router() {
        sessionData.location = [];
        if (location.hash.includes("-")) {
            sessionData.location = location.hash.split("-");
        } else {
            sessionData.location[0] = location.hash;
        }

        //////////////////////////////
        //SANDBOX
        /////////////////////////////

/*
        var d = new GDate().createFromData(22,10,2015);
        alert("this : "+ JSON.stringify(d));
        alert("toform:" + d.toForm());
        alert("toserver:" + d.toServer());

*/










        console.log("Entering router : " + sessionData.location[0]);
        
        if(!user && sessionData.location[0] != "#login" && sessionData.location[0] != "#register"){
            console.log("Starting recovery process");
            user = new LocalUser();
            var onSuccess = function(){
                console.log("recovery succeeded, calling router");
                router();
            }
            var onFail = function(){
                console.log("recovery failed, returning to login");
                location.hash = "login";
                router();
            }
            user.recoverFromCookie(onSuccess,onFail);
            /*
            tryRecoverDataFromCookies(
                function(){
                    //console.log("recovered:" + JSON.stringify(sessionData));
                    //location.hash = "dashboard";
                    router();
                },
                function(){ 
                    console.log("failed recover");
                    location.hash = "login";
                    router();
                }
            );*/
            console.log("recovery over, returning");
            return;
        }



        switch (sessionData.location[0]) {
            case "#login":
                loginView();
                break;
            case "#register":
                registerView();
                break;
            case "#dashboard":
                dashboardView();
                break;
            case "#myProjects":
                myProjects();
                break;
            case "#profileEdition":
                profileEdition();
                break;
            case "#userList":
                userList();
                break;
            default:
                //location.hash = defaultPage;

        }
        //history.pushState(null, null, location.hash);
    }

    window.onhashchange = router;



    function switchLang(lang) {
        sessionData["lang"] = lang;
        router();
    }
    function langSelector() {
        $("#langFr").click(function () {
            if (sessionData["lang"] != "fr") {
                switchLang("fr");
            }
        });
        $("#langEn").click(function () {
            if (sessionData["lang"] != "en") {
                switchLang("en");
            }
        });
    }
    function processDataForm(data) {
        for (var id in data) {
            data[id] = renderTpl("#" + data[id]["tpl"], data[id]);
        }
        return data;
    }
    function retrieveProjectList(criterias) {
        var retrievedData = [];
        $.ajax({
            url: '/Account/Login/', // register treatment url
            dataType: 'JSON',
            type: 'post',
            contentType: 'application/json',
            data: criterias,
            success: function (data, textStatus, jQxhr) {
                retrievedData = data;
            },
            error: function (jqXhr, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
        return retrievedData;
    }

    clickToLocation("#genyzLogo","#dashboard");
    langSelector();
});


	/*bindLoginSubmitButton();
	var u = $('#mainContainer').html();
	$('#mainContainer').html('');
	$('#mainContainer').html(u);
	bindLoginSubmitButton();*/

/*
function bindLoginSubmitButton(){
	$("#loginSubmitButton").click(function() {
		$('#mainContainer').fadeOut("fast", function() {
			$('#mainContainer').fadeIn("fast");
		});
	});
}*/