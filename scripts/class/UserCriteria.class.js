class UserCriteria {
    constructor(){
        
    }
    reset(){
        this.userType = -1;
        this.keyword = "";
        this.department = [];
        this.skill = [];
        this.highestDegree = [];
        this.seniority = [];
        this.availability = [];
        this.departmentInterest = [];
        this.skillInterest = [];
        this.status = [];
        this.country = -1;
        this.region = -1;
    }

    fromForm(){
        for(var field of UserCriteria.getFields()){
            this[field] = getVal(field);
        }
        console.log(this);
        return this;
    }
    toForm(tplData){
        for(var field of UserCriteria.getFields()){
            DropdownListData.toForm(field,tplData[field]);
        }
    }

    processUserList(data){
        var list = toObject(data);
        var filteredList = [];
        //console.log("PREPROC LIST :",list);
        var uc = this;
        $.each(list,function(k,usr){
            // keyword 
            if(uc.keyword){
                var regX = new RegExp(uc.keyword);
                if(!(regX.test(usr.firstName)  || regX.test(usr.lastName))){
                    return true;
                    console.log(usr.firstName, "-----" , usr.lastName , "-----" , uc.keyword);
                }
            }
            // dropdown lists

            if(uc.department){

            }


            filteredList.push(usr);
        });
        //console.log(filteredList)
        return filteredList;
    }
    static getFields(){
        return [
            "userType",
            "keyword",
            "department",
            "skill",
            "highestDegree",
            "seniority",
            "availability",
            "departmentInterest",
            "skillInterest",
            "status",
            "country",
            "region",
            "company"
        ];
    }
}