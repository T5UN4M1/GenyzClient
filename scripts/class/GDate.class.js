class GDate {
    constructor(id) {
        if(id){
            this.createDateFromFormData(id);
        } else {
            this.reset();
        }
    }

    reset() {
        this.year = 0;
        this.month = 0;
        this.day = 0;
        this.hour = 0;
        this.minute = 0;
        this.second = 0;
    }

    fromServer(data){
        var dat = new Date(data);
        this.createFromDateObject(dat);
        return this;
    }
    toServer(){
        var d = new Date(this.year,this.month-1,this.day,this.hour,this.minute,this.second);
        var str = d.format("isoDateTime");
        return str;
        //return this.getDateTime();
    }
    toForm(){ // TODO : improve this maybe
        var d = new Date(this.year,this.month-1,this.day,this.hour,this.minute,this.second);
        return (!this.isEmpty()) ? d.format("dd/mm/yyyy") : "";
        //return this.day + "/" + this.month + "/" + this.year;
    }
    createFromData (day,month,year){
        this.year = year;
        this.month = month;
        this.day = day;
        return this;
    }
    getDateTime() {
        var dateFormatter = "-";
        var separator = "T";
        var timeFormatter = ":";
        var res =
            this.year + dateFormatter + this.month + dateFormatter + this.day +
            separator +
            this.hour + timeFormatter + this.minute + timeFormatter + this.second;
        //console.log(res);
        return res;
    }
    getDateDisplay() {
        var formatter = "/";
        var str = day + formatter + month + formatter + day;
        return str;
    }
    getTimeDisplay() {
        var formatter = ":";
        var str = hour + formatter + minute + formatter + second;
        return str;
    }
    getDateTimeDisplay() {
        return this.getDateDisplay() + " " + this.getTimeDisplay();
    }
    createFromDateObject(date) {
        this.day = date.getDate();
        this.month = date.getMonth() + 1;
        this.year = date.getFullYear();
        this.hour = date.getHours();
        this.minute = date.getMinutes();
        this.second = date.getSeconds();
        return this;
    }
    createDateFromFormData(id) {
        var values = $("#" + id).val();
        var el = values.split("/");
        if(el.length > 0 && el.length < 4 && !isNaN(parseInt(el[0])) ){
            this.year = parseInt(el[2]);
            this.month = parseInt(el[1]);
            this.day = parseInt(el[0]);
            this.hour = "00";
            this.minute = "00";
            this.second = "00";
            return this;
        } else {
            this.reset();
            console.log("Warning : date input was invalid, resetting");
            return this;
        }
    }
    isEmpty(){
        return !(
            this.year != 0 ||
            this.month != 0 ||
            this.day != 0 ||
            this.hour != 0 ||
            this.minute != 0 ||
            this.second != 0
        );
    }
} 