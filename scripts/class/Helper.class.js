class Helper {
    // returns the object to be sent to the server
    static toServer(obj,mapping){
        var result = {};
        $.each(mapping,function(k,v){
            result[v] = obj[k];
        });
        return result;
    }
    // modifies obj with data according to mapping
    static fromServer(obj,data,mapping){
        //console.log(obj , " ----" , data , "-----" , mapping);
        $.each(mapping,function(k,v){
            obj[k] = data[v];
        });
        return obj;
    }
}