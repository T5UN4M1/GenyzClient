class UserData {
    constructor(){

    }
    reset(){
        this.userLevel = 0;

        this.firstName = "";
        this.lastName = "";
        this.birthday = "";
        this.nationalRegisterNumber = "";
        this.sex = 0;

        this.mailAddress = "";

        this.phone1 = "";
        this.phone2 = "";

        this.streetNumber = 0;
        this.streetName = "";
        this.postcode = 0;
        this.city = "";

        this.country = 0;
    }

    importProfileDataForm(){
        importFormData(this,[
            "firstName",
            "lastName",
            "nationalRegisterNumber",
            "sex",
            {
                type : "date",
                key : "birthday",
                id : "birth"
            }
        ]);
    }
    importContactDataForm(){
        importFormData(this,[
            "streetNumber",
            "streetName",
            "postcode",
            "city",
            "phone1",
            "phone2",
            "country"
        ]);
    }
    importConnectionDataForm(){
        importFormData(this,[
            
        ]
        
        
        )
    }
}