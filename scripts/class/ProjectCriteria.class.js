function ProjectCriteria(
    name,
    duration,
    statusId,
    departmentId,
    skills,
    languageId,
    budgetId,
    startDate,
    endDate
){
    this.name = name;
    this.duration = duration;
    this.statusId = statusId;
    this.departmentId = departmentId;
    this.skills = skills;
    this.languageId = languageId;
    this.budgetId = budgetId;
    this.startDate = startDate;
    this.endDate = endDate;
}

ProjectCriteria.prototype.createFromFormData = function(){
    this.name = $("#projectName").val();
    this.duration = $("#projectDuration").val() + "";
    this.statusId = $("#projectStatus").val();
    this.departmentId = $("#projectDepartment").val();
    this.skills = $("#projectSkills").val();
    this.languageId =  $("#projectLanguages").val();
    this.budgetId =  $("#projectBudget").val();
    this.startsDate = {
        date : dateFormToDateTime("projectStartDate"),
        side : $("#projectStartDate").val()
    };
    this.endsDate = {
        date : dateFormToDateTime("projectEndDate"),
        side : $("#projectEndDate").val()
    }
}

ProjectCriteria.prototype.isValid = function(project){
    function isCritRespected(key,project,crit){

        var isRespected = !( 
            key in crit && // on check d'abord que les criteres contiennent la key sinon c'est forcément respecté
            Object.keys(crit[key]).length > 0 && // meme si la key existe, il faut qu'on ait au moins un élément
            !crit[key].includes(project[key]+""));
        if(!isRespected){
            console.log("Project :"+ JSON.stringify(project) + " isnt valid for crit :" +JSON.stringify(crit)+" because of KEY : " + key);
        }
        return  isRespected;// on fait le check
    }
    if(!isCritRespected("departmentId",project,this) ||
    !isCritRespected("statusId",project,this) ||
    !isCritRespected("languageId",project,this) ||
    !isCritRespected("budgetId",project,this)
    ){
        //alert("project not valid");
        return false;
    }
    if(this.name != ""){
        var regX = new RegExp(this.name);
        if(!regX.test(project.name)){
            return false;
        }
    }
    return true;
}
ProjectCriteria.prototype.getCriteriaDisplayJson = function(){
    var data = [];

    if(this.name != ""){
        data.push({
            criteria : "Name must contain : "+this.name
        });
    }
    if(this.duration != ""){
        data.push({
            criteria : "Duration must be equal to : " + this.duration
        });
    }
    if(this.status != ""){
    }
    if(this.departmentId){
        data.push({
            criteria : "Departments : " + DropdownListData.generateDataList(commonData.departments,this.departmentId)
        });
    }
    if(this.skills){
        data.push({
            criteria : "Departments : " + DropdownListData.generateDataList(commonData.skills,this.skills)
        });
    }
    if(this.languageId){
        data.push({
            criteria : "Departments : " + DropdownListData.generateDataList(commonData.languages,this.languageId)
        });
    }
    if(this.budgetId){
        data.push({
            criteria : "Departments : " + DropdownListData.generateDataList(commonData.budgets,this.budgetId)
        });
    }
    if(this.startDate){

    }
    if(this.endDate){

    }
    return data;
}