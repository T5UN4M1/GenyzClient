class LoginToken{
    constructor(token,tokenType,expires){
        this.setData(token,tokenType,expires)
    }
    setData(token,tokenType,expires){
        this.token = token;
        this.tokenType = tokenType;
        this.expires = expires;
    }
    fromServer(data){
        this.token = data.access_token;
        this.tokenType = data.token_type;
        this.expires = data.expires_in;
        return this;
    }
    toCookie(){
        console.log(JSON.stringify(this));
        setCookie("token",JSON.stringify(this),999999);
        console.log("Saved token to cookies : " , this);
    }
    fromCookie(){
        var token = JSON.parse(getCookie("token"));
        console.log("Recovered token from cookies : " , token);

        this.token = token.token;
        this.tokenType = token.tokenType;
        this.expires = token.expires;
    }
}