class DropdownListData{
    static generateOptionListJson(data){
        var optionList = []
        $.each(data,function(k,v){
            if(!v){
                return true;
            }
            optionList.push({
                option : v.name,
                value : k
            });
        });
        return optionList;
    }

    static generateDataList(data,idList){
        var list = [];
        for(var id of idList){
            list.push(data[id].name);
        }
        //console.log(list);
        return list.join(", ");
    }
    static toForm(id,select){
        injectSelectedDataIntoJson(id,select);
    }
}