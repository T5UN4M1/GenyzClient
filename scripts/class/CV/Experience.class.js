class Experience {
    constructor(){
        this.reset();
    }

    reset(){
        this.jobTitle = "";
        this.company = "";
        this.location = "";
        this.startDate = new GDate();
        this.endDate = new GDate();
        this.description = "";
    }

    fromServer(data){
    }
    toServer(){

    }
    fromForm(id){
        if(elExist("jobTitle"+id)){
            this.jobTitle = getVal("jobTitle"+id);
            this.company = getVal("company"+id);
            this.location = getVal("location"+id);
            this.startDate = new GDate("startDate"+id);
            this.endDate = new GDate("endDate"+id);
            this.description = getVal("description"+id);
        }
    }
    toForm(id){
        var tplData = getDefaultTemplate("CVExperienceFields");
        console.log(tplData);
        tplData["CVExperienceJobTitle"]["value"] = this.jobTitle;
        tplData["CVExperienceCompany"]["value"] = this.company;
        tplData["CVExperienceLocation"]["value"] = this.location;
        tplData["CVExperienceStartDate"]["value"] = this.startDate.toForm();
        tplData["CVExperienceEndDate"]["value"] = this.endDate.toForm();
        tplData["CVExperienceProjectDescription"]["value"] = this.description;
        if(id){
            fixIds(tplData,id);
            tplData['id'] = id;
        }
            console.log(tplData);
        return tplData;
    }
    isEmpty(){
        return !(this.jobTitle != "" || this.company != "" || this.location != "" || this.description != "");
    }
}