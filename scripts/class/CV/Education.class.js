class Education {
    constructor(){
        this.reset();
    }

    reset(){
        this.name = "";
        this.schoolName = "";
        this.duration = 0;
        this.endDate = new GDate();
    }

    fromServer(data){

    }
    toServer(){

    }
    fromForm(id){
        if(elExist("name"+id)){
            this.name = getVal("name"+id);
            this.schoolName = getVal("schoolName"+id);
            this.duration = getVal("duration"+id);
            this.endDate = new GDate("endDate"+id);
        }
    }
    toForm(id){
        var tplData = getDefaultTemplate("CVEducation");
        tplData["CVEducationDegreeName"]["value"] = this.name;
        tplData["CVEducationSchoolName"]["value"] = this.schoolName;
        tplData["CVEducationDuration"]["value"] = this.duration;
        tplData["CVEducationEndDate"]["value"] = this.endDate.toForm();
        if(id){
            fixIds(tplData,id);
            tplData['id'] = id;
        }
            console.log(tplData);
        return tplData;

    }
    isEmpty(){
        return !(this.name != "" || this.schoolName != "" || this.duration != 0);
    }
}