class CV {
    constructor(){
        this.reset();
    }
    reset(){
        this.title = "";
        this.department = -1;
        this.seniority = -1;
        this.startDate = new GDate();
        this.skill = [];
        this.languages = new Languages();
        this.highestDegree = [];
        this.summary = "";

        this.education = {};
        this.experience = {};
        this.training = {};
    }
    getKey(field){
        switch(field){
            case "education":
            case "experience":
            case "training":
                var maxK = 0;
                $.each(this[field] , function(k,v){
                    if(parseInt(k)>maxK){
                        maxK = k;
                    }
                });
                return ++maxK;
            default : 
                console.log("Critical error, called getKey with wrong field name :" , field);
                return;
        }
    }
    insertEducation(education){
        var id = this.getKey("education");
        this.education[id] = education;
        return id;
    }
    insertTraining(training){
        var id = this.getKey("training");
        this.training[id] = training;
        return id;
    }
    insertExperience(experience){
        var id = this.getKey("experience");
        this.experience[id] = experience;
        return id;
    }
    fromForm(){
        if(elExist("educationList")){
            this.educationFromForm();
        } else if(elExist("trainingList")){
            this.trainingFromForm();
        } else if(elExist("experienceList")){
            this.experienceFromForm();
        }
    }
    educationFromForm(){
        this.highestDegree = getVal("highestDegree");
        $.each(this.education,function(k,v){
            v.fromForm(k);
        });
    }
    trainingFromForm(){
        $.each(this.training,function(k,v){
            v.fromForm(k);
        });
    }
    experienceFromForm(){
        this.summary = getVal("CVExperienceSummary");
        $.each(this.experience,function(k,v){
            v.fromForm(k);
        });
    }
}