class Training {
    constructor(){
        this.reset();
    }

    reset(){
        this.name = "";
        this.institutionName = "";
        this.duration = 0;
        this.endDate = new GDate();
    }

    fromServer(data){

    }
    toServer(){

    }
    fromForm(id){
        if(elExist("name"+id)){
            this.name = getVal("name"+id);
            this.institutionName = getVal("institutionName"+id);
            this.duration = getVal("duration"+id);
            this.endDate = new GDate("endDate"+id);
        }
    }
    toForm(id){
        var tplData = getDefaultTemplate("CVTraining");
        tplData["CVTrainingTrainingName"]["value"] = this.name;
        tplData["CVTrainingInstitutionName"]["value"] = this.institutionName;
        tplData["CVTrainingDuration"]["value"] = this.duration;
        tplData["CVTrainingEndDate"]["value"] = this.endDate.toForm();
        if(id){
            fixIds(tplData,id);
            tplData['id'] = id;
        }
            console.log(tplData);
        return tplData;

    }
    isEmpty(){
        return !(this.name != "" || this.institutionName != "" || this.duration != 0);
    }
}