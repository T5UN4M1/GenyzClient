class LocalUser extends User {
    constructor() {
        super();
        this.reset();
    }
    reset(){
        super.reset();
        this.token = new LoginToken();
        this.CVEdit = {} // CV being currently edited
    }
    toCookie() {
        setCookie("mail", this.mailAddress, 999999);
        this.token.toCookie();
        console.log("Saved mail address to cookies : " + this.mailAddress);
    }
    fromCookie() {
        this.mailAddress = getCookie("mail");
        console.log("Recovered mail address from cookies : " + this.mailAddress);
        this.token = new LoginToken();
        this.token.fromCookie();
    }
    loginFromData(mail,password,success,fail){
        this.mailAddress = mail;
        this.password = password;
        this.performLoginAction(success,fail);
    }
    loginFromForm(success, fail) {
        this.fromLoginForm();
        this.performLoginAction(success, fail);
    }
    fromLoginForm() {
        this.mailAddress = $("#mailAddress").val();
        this.password = $("#password").val();
    }
    login() {
        console.log("Login");
        return $.ajax({
            url: apiRoot + 'token', // login treatment url
            dataType: 'json',
            type: 'post',
            contentType: 'application/x-www-form-urlencoded',
            data: {
                "username": this.mailAddress,
                "password": this.password,
                "grant_type": "password"
            }
        }).done(function() {
            console.log("Login success");
        }).fail(function (jqXhr, textStatus, errorThrown) {
            console.log("Login failed");
        });
    }
    // token header must be set
    getUserLevel() {
        console.log("userlevel is called");
        var us = this;
        return ajaxGet('api/values')
            .done(function (data) {
                console.log("success userlevel");
                console.log(data);
                for (var obj of data) {
                    if (obj.type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/role") {
                        us.userLevel = roleStrToInt(obj.value);
                        console.log("User level : " + us.userLevel);
                    }
                }
            })
            .fail(function () {
                console.log("failure userlevel");
            });
    }
    performLoginAction(success, fail) {
        console.log("PerformLoginAction");
        var us = this;
        this.login()
            .done(function (data) {
                us.token = new LoginToken().fromServer(data);
                //alert("Setting request header with :" + JSON.stringify(sessionData.token));
                setRequestHeader(us.token.tokenType,
                     us.token.token);

                us.getUserLevel()
                    .done(function () {
                        us.toCookie();
                        success();
                    }).fail(function () {
                        alert("An unexpected error has occured, please retry to log in");
                        alertSessionData();
                    })
            }).fail(function () {
                console.log("Login failed");
                fail();
            });
    }
    recoverFromCookie(success,fail){
        this.fromCookie();
        console.log("trying to recover data");
        var us = this;
        if(this.mailAddress && this.token.token){
            setRequestHeader(us.token.tokenType,
                us.token.token);
            us.getUserLevel()
            .done(function(){
                console.log("Session recovery successful :" , us);
                success();
            }).fail(function(){
                console.log("Session recovery failed : " , us);
                fail();
            });
        }else{
            console.log("Cookie data couldn't be loaded, recovery failed");
            fail();
        }
    }
    isLoggedId(){
        return this.mailAddress && this.userLevel > 0;
    }
}