function Project(
    id,
    adminId,
    customerId,
    name,
    description,
    requiredProfile,
    statusId,
    startDate,
    duration,
    CVDeadline,
    departmentId,
    budgetId,
    areaId,
    skills
){
    this.id = id;
    this.adminId = adminId;
    this.customerId = customerId;
    this.name = name;
    this.description = description;
    this.requiredProfile = requiredProfile;
    this.statusId = statusId;
    this.startDate = startDate;
    this.duration = duration;
    this.CVDeadline = CVDeadline;
    this.departmentId = departmentId;
    this.budgetId = budgetId;
    this.areaId = areaId;
    this.skills = skills;

    // whatever ...
}
Project.prototype.reset = function(){
    this.id = -1;
    this.adminId = -1;
    this.customerId = -1;
    this.name = "";
    this.description =  "";
    this.requiredProfile = "";
    this.statusId = -1;
    this.startDate = "";
    this.duration = "";
    this.CVDeadline = "";
    this.departmentId = -1;
    this.budgetId = -1;
    this.areaId = -1;
    this.skills = [];
    this.languages = new Languages();
}

Project.prototype.createFromServerData = function(project){
    this.id = project.IdProject;
    this.adminId = project.IdAdmin;
    this.customerId = project.IdCustomer;
    this.name = project.Name;
    this.description = project.Description;
    this.requiredProfile = project.RequiredProfile;
    this.languages = new Languages().fromServer(project.Languages)
    this.statusId = project.IdStatus;
    this.startDate = project.StartingDate;
    this.duration = project.Duration;
    this.CVDeadline = project.DeadLineCV;
    this.departmentId = project.IdDepartment;
    this.budgetId = project.IdBudget;
    this.areaId = project.IdArea;
    this.skills = project.lstSkills;
    console.log(project);
}
Project.prototype.createFromFormData = function(noReset){
    if(!noReset){
        this.statusId = -1;
        this.id = -1;
        this.adminId = -1;
        this.customerId = -1;
    }
    this.name = $("#projectName").val();
    this.description = $("#projectDescription").val();
    this.requiredProfile = $("#projectProfileNeeded").val();
    this.languages = new Languages().fromForm();
    this.startDate = datepickerToDateTime("projectStartDate");
    this.duration = $("#projectDuration").val() + "";
    this.CVDeadline = datepickerToDateTime("projectCVDeadline");
    this.departmentId = $("#projectDepartment").val();
    this.budgetId = $("#projectBudget").val();
    this.areaId = $("#projectArea").val();
    this.skills = $("#projectSkills").val();
    console.log(this);
}
Project.prototype.getSendableProject = function(isNew){
    if(isNew){ // create new project
        console.log(this);
        return {
            Name : this.name,
            Description : this.description,
            RequiredProfile : this.requiredProfile,
            StartingDate : this.startDate,
            Duration : this.duration,
            DeadLineCV : this.CVDeadline,
            Languages : [],
            LanguageLevelForProjects : this.languages.toServer(),
            IdStatus : this.status,
            IdDepartment : this.departmentId,
            IdBudget : this.budgetId,
            IdArea : this.areaId,
            lstSkills : this.skills
        }
    } else {
        return { // update existing project // TODO :check if it works
            IdProject : this.id,
            IdAdmin : this.adminId,
            IdCustomer : this.customerId,
            IdStatus : this.statusId,
            
            Name : this.name,
            Description : this.description,
            RequiredProfile : this.requiredProfile,
            StartingDate : this.startDate,
            Duration : this.duration,
            DeadLineCV : this.CVDeadline,
            Languages : this.languages.toServer(),
            IdStatus : this.status,
            IdDepartment : this.departmentId,
            IdBudget : this.budgetId,
            IdArea : this.areaId,
            lstSkills : this.skills
        }
    }
}
Project.prototype.sendCreateProjectRequest = function(){
    console.log("Creating project : " , this.getSendableProject(true));
    return ajaxPost("api/projectsApi?email="+user.mailAddress,this.getSendableProject(true));    
}
Project.prototype.sendEditProjectRequest = function(){
    return ajaxPut("api/projectsApi/"+this.id,this.getSendableProject(false));
}

Project.prototype.setStatus = function(status){
    if(status > 0 && status < 5){
        this.status = status;
    } else {
        status = -1;
        console.log("Error changing status to "+status);
    }
}

Project.prototype.getProjectsFromServer = function(id){
    if(id){
        alert("trying");
        return ajaxGet("api/ProjectsAPI/"+ id);
    }
    switch(user.userLevel){
        case 2: // clients
            return ajaxGet("api/ProjectsAPI/?email=" + user.mailAddress)
            .done(function(data){
                console.log(data);
            });
        case 1:
        case 3:
        case 4:
            return ajaxGet("api/ProjectsAPI/");
        default:
            console.log("Error, bad userlevel");
    }
}

Project.prototype.parseProjectsFromServer = function(data){
    if(Array.isArray(data)){
        var projects = [];
        for(var proj of data){
            console.log(proj);
            var nProj = new Project();
            nProj.createFromServerData(proj);
            projects.push(nProj);
        }
        return projects;
    } else {
        var project = new Project();
        project.createFromServerData(data);
        return project;
    }
}

Project.prototype.getShortData = function(){
    //console.log(this);
    return {
        id : this.id,
        name : this.name,
        department : commonData["departments"][this.departmentId]["name"],
        duration : this.duration,
        startdate : formateDate(this.startDate),
        skills : DropdownListData.generateDataList(commonData.skills,this.skills),
        language : "wip"
    }
}
Project.prototype.getLongData = function(){
    return $.extend({},this.getShortData(),{
        description: this.description,
        requiredProfile : this.requiredProfile,
        status : commonData["status"][this.statusId]["name"],
        CVDeadline : formateDate(this.CVDeadline),
        budget : commonData["budgets"][this.budgetId]["name"],
        area : commonData["areas"][this.areaId]["name"]
    });
}
Project.prototype.getDefaultTplData = function(){
    //Languages = new Languages();
    var tplData = {
        projectName: getDefaultTemplate("projectName"),
        projectDuration: getDefaultTemplate("projectDuration"),
        projectStatus: getDefaultTemplate("projectStatus"),
        projectDepartment: getDefaultTemplate("projectDepartments"),
        projectSkills: getDefaultTemplate("projectSkills"),
        projectLanguages: getDefaultTemplate("projectLanguages"),
        projectBudget: getDefaultTemplate("projectBudgets"),
        projectStartDate: getDefaultTemplate("projectStartDate"),
        projectDescription: getDefaultTemplate("projectDescription"),
        projectRequiredProfile: getDefaultTemplate("projectRequiredProfile"),
        projectArea : getDefaultTemplate("projectAreas"),
        projectCVDeadline : getDefaultTemplate("projectCVDeadline")
    };
    console.log(tplData);
    return tplData;
}
Project.prototype.injectDataIntoForm = function(data){
    console.log(data);
    console.log(this);
    data.projectName.value = this.name;
    data.projectDuration.value = this.duration;
    injectSelectedDataIntoJson(this.statusId,data.projectStatus);
    injectSelectedDataIntoJson(this.departmentId,data.projectDepartment);
    injectSelectedDataIntoJson(this.skills,data.projectSkills);
    injectSelectedDataIntoJson(this.language,data.projectLanguages);
    injectSelectedDataIntoJson(this.budgetId,data.projectBudget);
    data.projectStartDate.value  = formateDate(this.startDate);
    data.projectDescription.value  = this.description;
    data.projectRequiredProfile.value  = this.requiredProfile;
    data.projectCVDeadline.value = formateDate(this.CVDeadline);
}
