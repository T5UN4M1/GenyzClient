class Availability {
    constructor(){
        this.reset();
    }
    reset(){
        this.type = 0; // type 1 : immediately, type 2 , under x weeks, type 3 : as from datetime
        this.addedData = ""; //  type 1 : null , type 2 : int , type 3 : GDate
    }
    fromForm(){
        var result = getRadioResult("availability");
        switch(result){
            case "1": // immediately
                this.type = 1;
                this.addedData = null;
                return this;
            case "2":
                this.type = 2;
                this.addedData = $("#availabilityUnder").val();
                return this;
            case "3":
                this.type = 3;
                this.addedData = new GDate("availabilityAsFrom");
                return this;
            default:
                console.log("not found :" + result);
                return this;
        }
    }
    toForm(data){
        console.log(data);
        var tbm =  data["type"+this.type];
        tbm["selected"]=true;
        switch(this.type){
            case 1 : return;
            case 2 :
                tbm["availabilityUnder"]["value"] = this.addedData;
                return;
            case 3:
                console.log(this.addedData);
                tbm["availabilityAsFrom"]["value"] = this.addedData.toForm();
        }
    }
    fromServer(data){
        var thisData;
        try{
            thisData = JSON.parse(data);
            this.type = thisData.type;
        } catch(e){
            this.type = 1;
        }
        
        switch(this.type){ 
            case 1: 
                this.addedData = null;
                break;
            case 2: 
                this.addedData = thisData.addedData;
                break;
            case 3: 
                this.addedData = new GDate().createFromData(thisData.addedData.day,thisData.addedData.month,thisData.addedData.year);
                break;
        }
        
        return this;
    }
    toServer(){
        return JSON.stringify(this);

    }
    getDefaultTemplate(){
    }
    getDisplay(){
        switch(this.type){
            case 1 : 
                return "Available immediately";
            case 2 : 
                return "Available under " + this.addedData + " weeks";
            case 3 :
                return "Available " + this.addedData.toForm();
        }
    }
}