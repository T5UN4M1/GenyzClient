class User {
    constructor() {
        this.reset();
    }
    reset() {
        this.id = 0;
        this.userLevel = 0;


        this.password = "";

        this.firstName = "";
        this.lastName = "";
        this.birthday = "";
        this.genderId = 0;

        this.mailAddress = "";

        this.phone1 = "";
        this.phone2 = "";

        this.streetNumber = 0;
        this.streetName = "";
        this.postcode = 0;
        this.city = "";

        this.country = 0;

        this.profileData = {}
        this.availability = new Availability();
    }

    fromServer(){
        var us = this;
        return ajaxGet("api/EmployeesAPI?email="+us.mailAddress).done(function(data){
            data = data[0];
            console.log(data);

            us.jobTitle = data.Function;
            us.id = data.IdUsers;
            us.streetName = data.Address;
            us.streetNumber = data.StreetNumber;
            us.availability.fromServer(data.Availability);
            us.birthday = new GDate().fromServer(data.Birthdate);
            us.CV = data.CV;
            us.city = data.City;
            us.country = data.Country;
            us.department = data.Department;
            us.firstName = data.Firstname;
            us.function = data.Function;
            us.genderId = data.IdGender;
            us.lastName = data.Name;
            us.phone1 = data.Phone1;
            us.postcode = data.PostalCode;
            us.countries = data.LstCountries;
            us.areas = data.LstAreas
            us.departments = data.LstDepartments;
            us.enabled = data.Enable;
            us.role = data.RoleID;
            us.genders = data.Gender;
            console.log(us);
        }).fail(function(){
            
        });
    }
    profileFromForm(){
        //this.fromServer();
        var vals = ["jobTitle","departmentInterest","skillInterest"]
        for(var val of vals){
            this[val] = $("#" + val).val();
        }
        this.availability.fromForm();
        console.log(this.availability);
        console.log(this);

    }
    profileToForm(data){
        data.jobTitle.value = this.jobTitle;
        console.log(this);
        injectSelectedDataIntoJson(this.departmentInterest,"departmentInterest");
        injectSelectedDataIntoJson(this.skillInterest,"skillInterest");
        this.availability.toForm(data.availability);
    }   
    personalDetailsToForm(data){
        var fields = ["firstName","lastName","phone1","phone2","nationalRegisterNumber"];
        //console.log(data);
        for(let field of fields){
            data[field].value = this[field];
        }
        data["birth"].value = this.birthday.toForm();


        data["address"]["streetNumberAndName"]["input"][0].value = this.streetName;
        data["address"]["streetNumberAndName"]["input"][1].value = this.streetNumber;

        data["address"]["postcodeAndCity"]["input"][0].value = this.postcode;
        data["address"]["postcodeAndCity"]["input"][1].value = this.city;
        DropdownListData.toForm(this.genderId,data["sex"]);
        console.log(data,this.genderId);


    }
    personalDetailsFromForm(){
        var fields = ["firstName","lastName","phone1","phone2","nationalRegisterNumber",
        "streetNumber","streetName","postcode","city"];

        for(let field of fields){
            this[field] = $("#"+field).val();
        }
        this.birthday = new GDate("birth");
        this.genderId =  $("#sex").val();
    }
    toServer(){
        var update = {
            IdUsers : this.id,
            Address : this.streetName,
            StreetNumber : this.streetNumber,
            Birthdate : this.birthday.toServer(),
            CV : this.CV,
            City : this.city,
            Country : this.country,
            Department : this.department,
            Firstname : this.firstName,
            Name : this.lastName,
            IdGender : this.genderId,
            Phone1 : this.phone1,
            PostalCode : this.postcode,
            Function : this.jobTitle,
            lstDepartmentsInterest : [],//this.departmentInterest,
            lstSkillsInterest : [],//this.skillInterest,
            Availability : this.availability.toServer(),
            lstCountries : [],// this.countries,
            lstAreas :[],// this.areas,
            lstDepartments : [],//this.departments,
            Enable : this.enabled,
            RoleID : this.role,
            EmployeeArea : null,
            EmployeeCustomers : null,
            EmployeeDepartment : null,
            EmployeeProject : null,
            EmployeeSkills : null,
            CountryHome : "Belgique",
            AspUserId : null,
            Skills : null,
            Phone2: null,
            NationalRegistryNumber : null,
            Image : null,
            Email : this.mailAddress,
            Gender : null // this.genders
        }

        console.log("UPDATING USER WITH : ",update,JSON.stringify(update));
        return ajaxPut("api/EmployeesAPI/"+this.id,JSON.stringify(update))
        .done(function(data){
            console.log("Sent data :", update);
            alert("updated");
        }).fail(function(data){
            console.log(data);
        });
    }
    passwordChangeFromForm(){
        var vals = ["password1","password2","oldPassword"]
        for(var val of vals){
            this[val] = $("#"+val).val();
        }
    }
    passwordChangeToServer(){
        var data = {
            OldPassword : this.oldPassword,
            NewPassword : this.password1,
            ConfirmPassword : this.password2
        }
        alert(JSON.stringify(data));
        console.log(this);
        return ajaxPost("api/Account/ChangePassword",JSON.stringify(data))
        .done(function(da){
            alert("password changed");
        }).fail(function(da){
            alert("Couldn't change your password.");
        })
    }





    static getUserList(role,action){
        switch(role){
            case 1: // admins
                return ajaxGet("api/AdminsAPI")
                .done(function(data){
                    console.log(data);
                    var list = [];
                    for(var usr of data){
                        list.push(new User().adminFromServer(usr));
                    }
                    console.log(list);
                    action(list,role);
                });
                break;
            case 2: // clients
                return ajaxGet("api/CustomersAPI?email="+user.mailAddress)
                .done(function(data){
                    console.log(data);
                    var list = [];
                    for(var usr of data){
                        list.push(new User().clientFromServer(usr));
                    }
                    console.log(list);
                    action(list,role);
                });
                break;
            case 3:// consultants
                return ajaxGet("api/EmployeesAPI?email="+user.mailAddress)
                .done(function(data){
                    console.log(data);
                    var list = [];
                    for(var usr of data){
                        var thatUser = new User().consultantFromServer(usr);
                        if(thatUser.userLevel == 3){
                            list.push(thatUser);
                        }
                    }
                    console.log(list);
                    action(list,role);
                });
                break;
            case 4: // candidats
                return ajaxGet("api/EmployeesAPI?email="+user.mailAddress)
                .done(function(data){
                    console.log(data);
                    var list = [];
                    for(var usr of data){
                        var thatUser = new User().consultantFromServer(usr);
                        if(thatUser.userLevel == 4){
                            list.push(thatUser);
                        }
                    }
                    console.log(list);
                    action(list,role);
                });
                break;
        }
    }
    getShortData(){
        var data = {
            id : this.id,
            userLevel : this.userLevel,
            firstName : this.firstName,
            lastName : this.lastName,
        }
        switch(this.userLevel){
            case 1 : 
                break;
            case 2 : // client
                
                
                break;
            case 3 : // consultant
            case 4 : // candidat
                data.seniority = commonData.budgets[this.seniority]["name"] + "(" + commonData.budgets[this.seniority]["gname"] + ")";
                data.department = DropdownListData.generateDataList(commonData.departments,this.department);
                data.experience = this.experience + " years experience";
                data.skill = DropdownListData.generateDataList(commonData.skills,this.skill);
                data.language = DropdownListData.generateDataList(commonData.languages,this.language);
                data.availability = this.availability.getDisplay();
                console.log(data);
                break;
        }
        return data;
    }
    adminFromServer(data){
       var obj = Helper.fromServer(this,data,User.getAdminMapping());
       this.userLevel = 1;
       return this;
    }
    consultantFromServer(data){
        var obj = Helper.fromServer(this,data,User.getConsultantMapping());

        this.userLevel = (data.RoleID == "Consultants") ? 3 : 4;

        var availability = this.availability;
        this.availability = new Availability().fromServer(availability);


        this.department = [];
        this.department.push(mt_rand(2,6));
        this.department.push(mt_rand(7,11));

        this.skill = [];
        this.skill.push(mt_rand(2,15));
        this.skill.push(mt_rand(16,30));
        this.skill.push(mt_rand(46,60));

        this.highestDegree = [];
        this.highestDegree.push(mt_rand(1,5));

        this.seniority  = mt_rand(1,4);

        this.departmentInterest = [];
        this.departmentInterest.push(mt_rand(2,6));
        this.departmentInterest.push(mt_rand(7,12));
        
        this.skillInterest = [];
        this.skillInterest.push(mt_rand(2,15));
        this.skillInterest.push(mt_rand(16,30));
        this.skillInterest.push(mt_rand(31,45));
        this.skillInterest.push(mt_rand(46,60));

        this.experience = mt_rand(3,15);

        this.language = []
        this.language.push(mt_rand(1,2)),
        this.language.push(3);



        return this;
    }
    clientFromServer(data){
        var obj = Helper.fromServer(this,data,User.getClientMapping());
        this.userLevel = 2;
        return this;
    }
    static getAdminMapping(){
        return {
            id : "IdAdmin",
            firstName : "Firstname",
            lastName : "Name",
            mailAddress : "Mail"
        }
    }
    static getClientMapping(){
        return {
            id : "IdUsers",
            firstName : "Firstname",
            lastName : "Name",
            gender : "IdGender",
            birthday : "Birthdate",

            mailAddress : "Mail",
            phone1 : "Phone1",
            phone2 : "Phone2",

            streetNumber : "StreetNumber",
            streetName : "Address",
            postcode : "PostalCode",
            city : "City",

            company : "Company",
            enable : "Enable"
        }
    }
    static getConsultantMapping(){
        return {
            id : "IdUsers",
            firstName : "Firstname",
            lastName : "Name",
            gender : "IdGender",
            birthday : "Birthdate",

            mailAddress : "Mail",
            phone1 : "Phone1",
            phone2 : "Phone2",

            streetNumber : "StreetNumber",
            streetName : "Address",
            postcode : "PostalCode",
            city : "City",

            nationalRegistryNumber : "NationalRegistryNumber",
            function : "Function",

            availability : "Availability",
            
            enable : "Enable"
        }
    }


    registerFromForm(userLevel){
        var baseData = {
            "Firstname": getVal("firstName"),
            "Name": getVal("lastName"),
            "Email": getVal("mail"),  
            "Password": getVal("password1"),
            "ConfirmPassword": getVal("password2"),
            "IdGender": getVal("sex"),      
            "Phone1": getVal("phone1"),
            "Phone2": getVal("phone2"),   
            "Birthdate": new GDate("birth").toServer(),
            "Image" : getDefaultAvatar(getVal("sex")),
            "Function": getVal("jobTitle")
        }
        if(userLevel == 1){
            var adminRegister = {
                "IdAdmin": 0,
                "AspUserId": null,
                "Genders": null
            }
            var finalData = arrayMerge(adminRegister,baseData);
            console.log(finalData);
            return ajaxPost("api/Account/RegisterAdmin",JSON.stringify(finalData));
        }


        var departments = getVal("departmentInterest");
        var listDepartment = []
        if(departments){
            $.each(departments,function(k,v){
                listDepartment.push({IdDepartment:v});
            });
        }
        
        var skills = getVal("skillInterest");
        var listSkill = []
        if(skills){
            $.each(skills,function(k,v){
                listSkill.push({IdSkills:v});
            });
        }
        if(userLevel == 2){
            var customerRegister = {
                "ActivityDescription": getVal("activity"),
                "Company": getVal("company"),
                "Sector": getVal("sector"),
                "HisDepartment": getVal("departmentName"),
                "AspUserId": null,
                "LoggedOn": null,
                "RoleName":"Customers",
                "IdArea": getVal("region"),
                "IdCountry": getVal("country"),
                "Areas": null,
                "Project": null,
                "CustomersDepartment":listDepartment,
                "CustomersSkills":listSkill,
                "EmployeeCustomers": null,
                "lstDepartments": [],
                "lstSkills": [],
                "IdUsers":0,
                "Address": getVal("streetName"),
                "StreetNumber": getVal("streetNumber"),
                "PostalCode": getVal("postcode"),
                "City": getVal("city"),
                "CountryHome": getVal("countryField"),
                "Enable": true,
                "Department": null,
                "Skills": null,
                "Gender": null
            }
            var finalData = arrayMerge(baseData,customerRegister);
            console.log(finalData);
            return ajaxPost("api/Account/RegisterCustomers",JSON.stringify(finalData));
        }  



        var consultantRegister = {
            "NationalRegistryNumber": getVal("nationalRegisterNumber"),
            "Availability": new Availability().fromForm().toServer(),
            "RoleID": "Consultants",
            "AspUserId": null,
            "CV": null,
            "Country": getVal("country"),
            "EmployeeArea": [{"IdArea":getVal("region")}],
            "EmployeeDepartment":listDepartment,
            "EmployeeProject": null,
            "EmployeeSkills":listSkill,
            "EmployeeCustomers": null,
            "lstDepartments": [],
            "lstDepartmentsInterest": [],
            "lstSkillsInterest": [],
            "lstAreas": [],
            "lstCountries": [],
            "IdUsers":0,
            "Address": getVal("streetName"),
            "StreetNumber": getVal("streetNumber"),
            "PostalCode": getVal("postcode"),
            "City": getVal("city"),
            "CountryHome": getVal("countryField"),
            "Enable": true,
            "Department": null,
            "Skills": null,
            "Gender": null
        }
        var finalData = arrayMerge(baseData,consultantRegister);
        console.log(finalData);
        switch(userLevel){
            case 3:
                return ajaxPost("api/Account/RegisterConsultants",JSON.stringify(finalData));
            case 4:
                return ajaxPost("api/Account/RegisterCandidates",JSON.stringify(finalData));
            case 5:
                return ajaxPost("api/Account/RegisterNewCandidates",JSON.stringify(finalData));
        }
        
        
    }

}