function loginView() {
    var loginFormData = {
        mailAddress : {
            label: getLang().mailAddress,
            id: "mailAddress"
        },
        password : {
            label: getLang().password,
            id: "password",
            type: "password"
        },
        submit : {
            label : getLang().submit,
            id: "loginSubmit"
        },
        loginConsultant : {
            label : "login consultant",
            id: "loginConsultant"
        },
        loginClient : {
            label : "login client",
            id: "loginClient"
        },
        loginAdmin : {
            label : "login admin",
            id: "loginAdmin"
        },
        login : getLang().login

    }

    $("#mainContainer").html($.render.login(loginFormData));


    $("#loginSubmit").click(function () {
        // TODO check form fields
        if(true){
            user = new LocalUser();
            var success = function(){
                location.hash = "#dashboard";
            };
            var fail = function(){
                alert("login failed");
            }
            user.loginFromForm(success,fail);
        } else {
            
            var email = $("#mailAddress").val();
            var pass = $("#password").val();
            
            performLoginAction(email,pass,function(){
                location.hash = "#dashboard";
            });
        }
    });

    $("#loginConsultant").click(function () {
            user = new LocalUser();
            var success = function(){
                location.hash = "#dashboard";
            };
            var fail = function(){
                alert("login failed");
            }
            user.mailAddress = "consultants2@hotmail.com";
            user.password = "Test1234$"
            user.performLoginAction(success,fail);
    });
    $("#loginClient").click(function () {
        user = new LocalUser();
        var success = function(){
            location.hash = "#dashboard";
        };
        var fail = function(){
            alert("login failed");
        }
        user.mailAddress = "customers4@hotmail.com";
        user.password = "Test1234$"
        user.performLoginAction(success,fail);
    });
    $("#loginAdmin").click(function () {
        user = new LocalUser();
        var success = function(){
            location.hash = "#dashboard";
        };
        var fail = function(){
            alert("login failed");
        }
        user.mailAddress = "admin@admin.com";
        user.password = "Test1234$";
        user.performLoginAction(success,fail);
    });
}