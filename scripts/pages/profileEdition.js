function profileEdition() {
    function makeNavBar() {
        clickToLocation("#editProfileButton", "#profileEdition-profile");
        clickToLocation("#selectCVButton", "#profileEdition-selectCV");
        clickToLocation("#editConnectionDataButton", "#profileEdition-connectionData");
        clickToLocation("#editContactDataButton", "#profileEdition-contactData");
    }



    var profileEditionData = {
        topNavBar: {
            buttons: [
                {
                    id: "editProfileButton",
                    label: "My profile"
                },
                {
                    id: "selectCVButton",
                    label: "My CVs"
                },
                {
                    id: "editContactDataButton",
                    label: "My details"
                },
                {
                    id: "editConnectionDataButton",
                    label: "My settings"
                },

            ]
        },
        editProfile: {
            profileEdition: "Profile edition",
            profileSubmit: {
                id: "editProfileSubmit",
                label: "Submit"
            },
            availability : getDefaultTemplate("availability"),
            departmentInterest : getDefaultTemplate("departmentInterest"),
            skillInterest : getDefaultTemplate("skillInterest"),
            location : getDefaultTemplate("location"),
            jobTitle : getDefaultTemplate("jobTitle"),
            changeAvatar : {
                id:"changeAvatar",
                label : "Change avatar",
                class : "genyzLightButton"
            },
            avatar : "defaultAvatarM"
            /*
            firstName : getDefaultTemplate('firstName'),
            lastName : getDefaultTemplate("lastName"),
            birth : getDefaultTemplate("birth"),
            nationalRegisterNumber : getDefaultTemplate("nationalRegisterNumber"),
            sex : getDefaultTemplate("sex")*/

        },

        editConnectionData: {
            connectionData: "Connection data",
            submit1: {
                id: "submitMailChange",
                label: "Submit"
            },
            submit2: {
                id: "submitPasswordChange",
                label: "Submit"
            },
            mail : getDefaultTemplate("mail"),
            password1 : getDefaultTemplate("password1"),
            password2 : getDefaultTemplate("password2"),
            oldPassword : getDefaultTemplate("oldPassword")
        },
        editContactData: {
            contactData: "Contact data",
            contactDataSubmit: {
                id: "personalDetailsSubmit",
                label: "Submit"
            },
            page1 : {
                id:"page1",
                label:"Next"
            },
            page2 : {
                id:"page2",
                label:"Back"
            },
            phone1 : getDefaultTemplate("phone1"),
            phone2 : getDefaultTemplate("phone2"),
            address : getDefaultTemplate("address"),
            country : getDefaultTemplate("country"),

            firstName : getDefaultTemplate('firstName'),
            lastName : getDefaultTemplate("lastName"),
            birth : getDefaultTemplate("birth"),
            nationalRegisterNumber : getDefaultTemplate("nationalRegisterNumber"),
            sex : getDefaultTemplate("sex")
        },
        selectCV: {
            CVEditionNavbar: [

            ],
            CVs: [

            ],
            newCV: {
                id: "newCV",
                placeholder: "New CV name"
            },
            newCVSubmit: {
                id: "newCVSubmit",
                label: "Create a new CV"
            }
        }

    }
    console.log(profileEditionData);
    var mainPages = ["editProfile", "selectCV", "editContactData", "editConnectionData"];
    var cvPages = [];
    /*
    function hideMainPages(){
        for(var p in mainPages){
            $("#"+p).hide();
        }
    }*/
    var selectedClass = "genyzNavButtonSelected";
    switch (sessionData.location[1]) {
        case "connectionData":
            profileEditionData["topNavBar"]["buttons"][3]["class"] = selectedClass;
            $("#mainContainer").html($.render.editConnectionData(profileEditionData));
            makeNavBar();
            $("#submitPasswordChange").click(function(){
                user.passwordChangeFromForm();
                user.passwordChangeToServer();
            })
            break;
        case "contactData":
            user.fromServer().done(function(){
                profileEditionData["topNavBar"]["buttons"][2]["class"] = selectedClass;
                user.personalDetailsToForm(profileEditionData.editContactData);
                $("#mainContainer").html($.render.editContactData(profileEditionData));
                $(".datepicker").datepicker({});
                makeNavBar();

                var p = ["#det1","#det2"];
                var t = 200;
                $("#page1").click(function(){
                    $(p[0]).slideUp(t);
                    $(p[1]).delay(t).show(t);
                });
                $("#page2").click(function(){
                    $(p[1]).hide(t);
                    $(p[0]).delay(t).slideDown(t);
                });
                $(p[1]).hide();
                $("#personalDetailsSubmit").click(function(){
                    console.log(user);
                    user.personalDetailsFromForm();
                    console.log(user);
                    user.toServer().done(function(){
                        console.log("updated user profile");
                    });
                });
            })

            break;
        case "profile":
            console.log(profileEditionData);
            user.fromServer().done(function(){
                profileEditionData["topNavBar"]["buttons"][0]["class"] = selectedClass;
                user.profileToForm(profileEditionData["editProfile"]);
                console.log(profileEditionData["editProfile"]);
                $("#mainContainer").html($.render.editProfile(profileEditionData));
                $("#birth").datepicker({});
                $("#availabilityAsFrom").datepicker({});
                $('.selectpicker').selectpicker('refresh');
                makeNavBar();
                regionPicker("country","region");
                $("#editProfileSubmit").click(function(){
                    user.profileFromForm();
                    console.log(user);
                    user.toServer().done(function(){
                        console.log("updated user profile");
                     });
                });
            }).fail(function(){

            });
            break;
        case "selectCV":
            console.log("sending");
            ajaxGet('api/CVsAPI?email=' + user.mailAddress, {})
                .done(function (data) {
                    console.log(data);
                    userCVS = data;
                    var TBI = [];
                    for (var cv of userCVS) {
                        var newCV = {
                            title: cv.Title,
                            loadCVButton: {
                                id: "loadCV" + cv.IdCV,
                                label: "Edit",
                                class: "genyzLightButton"
                            },
                            deleteCVButton: {
                                id: "deleteCV" + cv.IdCV,
                                label: "Delete",
                                class: "genyzLightButton"
                            },
                            copyCVButton : {
                                id: "copyCV" + cv.IdCV,
                                label: "Create a copy",
                                class: "genyzLightButton"
                            }
                        }
                        TBI.push(newCV);
                    }
                    profileEditionData.selectCV.CVs = TBI;
                    profileEditionData["topNavBar"]["buttons"][1]["class"] = selectedClass;
                    $("#mainContainer").html($.render.selectCV(profileEditionData));
                    makeNavBar();
                    $("#newCVSubmit").click(function(){
                        user.CVEdit = new CV();
                        $("#CVSelect").hide(200);
                        function up(step){
                            user.CVEdit.fromForm();
                            updateNavBarData(step);
                            updateContentData(step);
                            $("#CVEditionNavBar").finish().fadeOut(300).fadeIn(300);
                            $("#CVEditionContent").finish().hide(100).slideDown(1000);
                        }
                        function updateNavBarData(step){
                            var baseData = {
                                buttons: [
                                    {
                                        id: "CVGeneral",
                                        label: "1. General"
                                    },
                                    {
                                        id: "CVEducation",
                                        label: "2. Education"
                                    },
                                    {
                                        id: "CVTraining",
                                        label: "3. Trainings"
                                    },
                                    {
                                        id: "CVExperience",
                                        label: "4. Experience"
                                    }
                                ]
                            }
                            for(let i in baseData["buttons"]){
                                
                                baseData["buttons"][i]["class"] = ((parseInt(i)+1)!=step) ? "lightNavBarButton" : "lightNavBarButtonSelected";
                            }
                            $("#CVEditionNavBar").html($.render.buttonGroup(baseData));
                            $("#CVEditionNavBar").show();
                            $.each(baseData["buttons"] , function(k,v){
                                $("#"+v.id).click(function(){
                                    up(k+1);
                                })
                            });
                        }
                        function updateContentData(step){
                            var generalData = {
                                saveDraft : {
                                    id:"saveDraft",
                                    label:"Save draft",
                                    class:"genyzLightButton"
                                },
                                next : {
                                    id:"next",
                                    label:"Next page"
                                },
                                submit : {
                                    id: "submit",
                                    label : "Submit"
                                }
                            };

                            
                            function determineScope(){
                                var scopes = {
                                    education : 1,
                                    training : 2,
                                    experience : 3
                                }
                                var item;
                                $.each(scopes , function(k,v){
                                    if(elExist(k+"List")){
                                       item = k;
                                       return false;
                                    }
                                });
                                if(item){
                                    return item;
                                } else {
                                    console.log("Critical error, couldn't determine scope");
                                    return null;
                                }
                                
                            }
                            function postRender(id){
                                switch(String(determineScope())){
                                    case "education":
                                        $("#endDate"+id).datepicker({});
                                        break;
                                    case "training":
                                        $("#endDate"+id).datepicker({});
                                        break;
                                    case "experience":
                                        $("#startDate"+id).datepicker({});
                                        $("#endDate"+id).datepicker({});
                                        autoBulletPointsTextarea('description'+id);
                                        break;
                                }
                            }
                            // delete a form from the dom and active CV
                            function deleteRow(id){
                                var scope = determineScope();

                                // if row is deleted, we have to delete the corresponding entry in the active CV being edited
                                delete user.CVEdit[scope][id];

                                // now deleting it from the DOM
                                var lineId = "#"+scope+"Line"+id;
                                $(lineId).slideUp(200);
                                setTimeout(function(){
                                    $(lineId).remove();
                                },200);
                            }
                            // adds a blank form to the dom and an entry to the active CV
                            function addRow(){
                                var scope = determineScope();

                                switch(String(scope)){
                                    case "education":
                                        // first , we create the element
                                        var education = new Education();
                                        // then save it into active CV
                                        var id = user.CVEdit.insertEducation(education);
                                        // finally insterting it in the dom
                                        insertLine(id,true);
                                        break;
                                    case "training": 
                                        var training = new Training();
                                        var id = user.CVEdit.insertTraining(training);
                                        insertLine(id,true);
                                        break;
                                    case "experience":
                                        var experience = new Experience();
                                        var id = user.CVEdit.insertExperience(experience);
                                        insertLine(id,true);
                                        break;
                                }
                            }
                            // inserts an element to the dom from an existing element in the active CV
                            function insertLine(id,edit){
                                var scope = determineScope();
                                var Scope = firstCharToUpperCase(scope);
                                //alert("Inserting " +scope+ " with id :"+id);

                                var tplData = user.CVEdit[scope][id].toForm(id);

                                tplData['edit'] = edit;

                                $("#"+scope+"List").append($.render['CV'+Scope+'Item'](tplData));
                                postRender(id);
                                $("#"+scope+"Line"+id).hide().slideDown(200);
                                $("#delete"+id).click(function(){
                                    deleteRow(id);
                                });
                                $("#update"+id).click(function(){
                                    toggleLine(id);
                                });
                            }

                            function toggleLine(id){
                                var scope = determineScope();
                                var Scope = firstCharToUpperCase(scope);
                                var edit = true;
                                // first, we detect what's the current mode for this line
                                // name for education and training , title for experience

                                var isForm = elExist("name"+id) || elExist("jobTitle"+id);

                                if(isForm){
                                    // is currently in form mode
                                    user.CVEdit[scope][id].fromForm(id);
                                    edit = false;
                                }
                                    // if it's in line mode, we don't have to do anything,
                                    // edit is already set to true and we don't have to call fromForm

                                // switching templates
                                var tplData = user.CVEdit[scope][id].toForm(id);
                                tplData["edit"] = edit;
                                $("#"+scope+"Line"+id).replaceWith($.render["CV"+Scope+"Item"](tplData));
                                // if we were not in form mode before toggle, we are now in form mode, and we need to apply post render
                                if(!isForm){
                                    postRender(id);
                                }
                                // events for buttons
                                $("#update"+id).click(function(){
                                    toggleLine(id)
                                });
                                $("#delete"+id).click(function(){
                                    deleteRow(id)
                                });
                            }
                            switch(step){
                                case 1: // general
                                    var req = ["CVTitle","CVDepartment","CVSeniority",
                                    "CVExperience","CVSkill","CVLanguage"];
                                    for(let r of req){
                                        generalData[r] = getDefaultTemplate(r);
                                    }
                                    $("#CVEditionContent").html($.render.CVGeneral(generalData));
                                    $("#startDate").datepicker({
                                        format: "yyyy",
                                        minViewMode: 2,
                                        startDate : "-25y",
                                        endDate : "+0y"
                                    });

                                    $('.selectpicker').selectpicker('refresh');

                                    activateLanguageSelection();
                                    $("#next").click(function(){
                                        ++step;
                                        up(step);
                                    });
                                    break;
                                case 2: // education
                                    var req = ["CVDegree","CVAddEducation"];
                                    for(let r of req){
                                        generalData[r] = getDefaultTemplate(r);
                                    }
                                    $("#CVEditionContent").html($.render.CVEducation(generalData));
                                    $("#highestDegree").selectpicker("refresh");
                                    //console.log(user.CVEdit);
                                    if(Object.keys(user.CVEdit.education).length > 0){
                                        $.each(user.CVEdit.education,function(k,v){
                                            insertLine(k,v.isEmpty());
                                        });
                                    } else {
                                        addRow();
                                    }
                                    
                                    $("#addEducationForm").click(function(){
                                        addRow();
                                    });
                                    $("#next").click(function(){
                                        ++step;
                                        up(step);
                                    });
                                    break;
                                case 3:
                                    var req = ["CVAddTraining"];
                                    for(let r of req){
                                        generalData[r] = getDefaultTemplate(r);
                                    }
                                    $("#CVEditionContent").html($.render.CVTraining(generalData));
                                    
                                    if(Object.keys(user.CVEdit.training).length > 0){
                                        $.each(user.CVEdit.training,function(k,v){
                                            insertLine(k,v.isEmpty());
                                        });
                                    } else {
                                        addRow();
                                    }
                                    $("#addTrainingForm").click(function(){
                                        addRow();
                                    });
                                    $("#next").click(function(){
                                        ++step;
                                        up(step);
                                    });
                                    break;
                                case 4:
                                    var req = ["CVExperienceSummary","CVAddExperience"];
                                    for(let r of req){
                                        generalData[r] = getDefaultTemplate(r);
                                    }
                                    var req = ["CVAddExperience"];
                                    for(let r of req){
                                        generalData[r] = getDefaultTemplate(r);
                                    }

                                    $("#CVEditionContent").html($.render.CVExperience(generalData));
                                    autoBulletPointsTextarea('summary');


                                    if(Object.keys(user.CVEdit.experience).length > 0){
                                        $.each(user.CVEdit.experience,function(k,v){
                                            insertLine(k,v.isEmpty());
                                        });
                                    } else {
                                        addRow();
                                    }
                                    $("#addExperienceForm").click(function(){
                                        addRow();
                                    });
                                    $("#next").click(function(){
                                        ++step;
                                        up(step);
                                    });
                                    break;
                            }

                        }
                        var step = 1;
                        $("#CVEditionTitle").html("New CV");
                        updateNavBarData(step);
                        updateContentData(step);
                        $("#CVEditionNavBar").hide().show(200);
                    });
                }).fail(function () {
                    alert("error");
                });






            break;
        default:
            location.hash = "#profileEdition-profile";
            break;
    }


}