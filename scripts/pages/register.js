function registerView() {
    var registerForm = {

        firstName: tpl("firstName"),
        lastName: tpl("lastName"),
        birthdate: tpl("birth"),
        address: tpl("address"),/*{
            "streetNumberAndName": {
                "firstId": "streetName",
                "label": getLang().streetNumberAndName,
                "input": [
                    { "mobileWidth": 3, "desktopWidth": 3, "id": "streetNumber", "placeholder": getLang().streetNumber },
                    { "mobileWidth": 9, "desktopWidth": 9, "id": "streetName", "placeholder": getLang().streetName }
                ]
            },
            "postcodeAndCity": {
                "firstId": "postcode",
                "label": getLang().streetNumberAndName,
                "input": [
                    { "mobileWidth": 3, "desktopWidth": 3, "id": "postcode", "placeholder": getLang().postcode },
                    { "mobileWidth": 9, "desktopWidth": 9, "id": "city", "placeholder": getLang().city }
                ]
            }
        },
        "countrySelect": {
            "id" : "countrySelect",
            "label": getLang().country,
            "options": [
                { "value": "be", "option": "Belgium" },
                { "value": "fr", "option": "France" }
            ]
        },*/
        mail : tpl("mail"),
        country : mkSimple("countryField","Country"),
        sex : tpl("sex"),

        jobTitle : mkSimple("jobTitle","Job title"),

        company : tpl("company"),
        companyLocation : tpl("location"),

        department : tpl("department"),



        sector : mkSimple("sector","Sector"),

        jobTitleConsultant : tpl("jobTitle"),
        departmentInterest : tpl("departmentInterest"),
        skillInterest : tpl("skillInterest"),
        location : tpl("location"),
        availability : tpl("availability"),

        departmentName : mkSimple("departmentName","Name of your department"),
        departmentInterestClient : {
            label : "What is your department’s area(s) of expertise?",
            id : "departmentInterest",
            options : DropdownListData.generateOptionListJson(commonData.departments)
        },
        skillInterestClient : {
            label : "Which competencies are you looking for in general?",
            id : "skillInterest",
            options : DropdownListData.generateOptionListJson(commonData.skills)
        },
        activity : {
            id:"activity",
            label:"Please describe your activity",
            rows:4
        },

        password1: {
            id: "password1",
            type: "password",
            label: getLang().password
        },
        password2: {
            type: "password",
            id: "password2",
            label: getLang().passwordConfirmation
        },
        nationalRegisterNumber: mkSimple("nationalRegisterNumber", getLang().nationalRegisterNumber),

        phone1: mkSimple("phone1",getLang().phoneNumber),
        phone2: mkSimple("phone2",getLang().secondPhoneNumber),

        submit: mkButton("submit","Submit"),
        back1: mkButton("back1","back",true),
        back2: mkButton("back2","back",true),
        next1: mkButton("next1","next"),
        next2: mkButton("next2","next")
    }

    registerForm.mail.label = "Email address";
    registerForm.birthdate.fullLength = true;
    var TBS;
    if(sessionData.location[1]){
        TBS = sessionData.location[1];
    }

    // making sure that you cant register as admin freely, 
    // but it still isnt that safe cuz
    // anybody who can read the code can still change URL and register
    var map = {
        1 : "klG08gLL23",  // code admin
        2 : "sfoFD0Fib9S", // code client
        3 : "sdkf82vL2dZ", // code consultant
        4 : "plx93XoGhE33", // code candidat
        5 : "ozEP301cGZaze" // code newCandidat
    }
    // register links :  register-{{code}}
    // example for admins : genyz.be/xpertyz/#register-klG08gLL23

    var ok = false;
    $.each(map , function(k,v){
        if(TBS == v){
            TBS = parseInt(k);
            ok = true;
            return false;
        }
    })
    if(!ok){
        TBS = 5;
    }
    registerForm.userLevel = TBS;

    

    // required fields 
    
    var requiredForm = ["firstName","lastName","birthdate","mail",
    "phone1","sex","jobTitle","jobTitleConsultant","password1","password2",
    "location","availability","departmentInterest","skillInterest",
    "country","company","companyLocation","sector",
    "departmentName","skillInterestClient","departmentInterestClient","activity"];
    
    registerForm.companyLocation.label = "Region"

    for(var field of requiredForm){
        if(field != "birthdate"){
            registerForm[field].placeholder = registerForm[field].label;
        }
        registerForm[field].label += "*";
    }

    registerForm.address.streetNumberAndName.label += "*";
    registerForm.address.postcodeAndCity.label += "*";


    //var data = processDataForm(registerForm);
    //data["page"] = renderTplTo("#register", addLangToData(registerForm), "#mainContainer");

    $("#mainContainer").html($.render.register(registerForm));

    $(".selectpicker").selectpicker("refresh");
    $(".datepicker").datepicker({});
    regionPicker("country","region");


    $("#page1").hide();
    $("#page2").hide();
    $("#page3").hide();

    if(TBS == 2){ // for customers, we want to display country only if area is set to international or europe
        $("#countrySelection").hide();
        $("#country").on("change",function(){
            if(getVal("country") == 3 || getVal("country") == 4){
                $("#countrySelection").slideDown(200);
            } else {
                $("#countrySelection").slideUp(200);
            }
        });
    }
    function displayPage(id){
        var pageAmount = 3;
        for(var i=1;i<= pageAmount;++i){
            $("#page"+i).fadeOut(200);
        }
        $("#page"+id).delay(200).fadeIn(200);
    }
    displayPage(1);

    $("#next1").click(function () {
        displayPage(2);
    });
    $("#next2").click(function () {
        displayPage(3);
    });
    $("#back1").click(function () {
        displayPage(1);
    });
    $("#back2").click(function () {
        displayPage(2);
    });
    var errorMsg;

    $("#submit").click(function () {
        if ($("#pwd1").val() != $("#pwd2").val()) {
            /*$("#pwd1").attr("data-toggle","tooltip").attr("title","The passwords you entered are different").attr("data-placement","right"));
            $('#pwd1').tooltip({trigger:"manual",container:"body"});
            $("#pwd1").tooltip("toggle"); */ // doesnt work for some reasons

        }
        var usr = new User();
        usr.registerFromForm(TBS)
        .done(function(){
            user = new LocalUser();
            user.loginFromData(getVal("mail"),getVal("password1"),
        
            function(){ // success
                alert("You are registered, you will be redirected to your dashboard page");
                location.hash = "#dashboard";
            },
            function(){ // fail
                alert("You are registered.")
                location.hash = "#login"
            }   
        );
        })
        .fail(function(){
            alert("Registration failed");
        })
        /*
        $.ajax({
            url: '/Account/Login/', // register treatment url
            dataType: 'JSON',
            type: 'post',
            contentType: 'application/json',
            data: registerFormData,
            success: function (data, textStatus, jQxhr) {
                alert("ok");
            },
            error: function (jqXhr, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
        */
    });
}