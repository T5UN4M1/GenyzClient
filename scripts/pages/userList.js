function userList() {
    var userList = [];
    function getUserCriteriaTplData(){

        var tpl =   {
            resetCriteria: {
                id: "resetCriteria",
                label: "Reset criteria"
            },
            userType: {
                id: "userType",
                label: "User type",
                narrow: true,
                options: DropdownListData.generateOptionListJson(commonData.roles),
            },
            keyword: {
                id: "keyword",
                label: "Keywords",
                narrow : true
            },
            department: {
                id: "department",
                label: "Areas of expertise",
                options: DropdownListData.generateOptionListJson(commonData.departments),
                narrow: true
            },
            skill: {
                id: "skill",
                label: "Competencies",
                options: DropdownListData.generateOptionListJson(commonData.skills),
                narrow: true
            },
            highestDegree: {
                id: "highestDegree",
                label: "Education level",
                options: DropdownListData.generateOptionListJson(commonData.degreesTypes),
                narrow: true
            },
            seniority: {
                id: "seniority",
                label: "Seniority",
                options: DropdownListData.generateOptionListJson(commonData.budgets),
                narrow: true
            },
            availability: {
                id: "availability",
                label: "Availability",
                alone: true
            },
            departmentInterest: {
                id: "departmentInterest",
                label: "Department(s)",
                options: DropdownListData.generateOptionListJson(commonData.departments),
                narrow: true
            },
            skillInterest: {
                id: "skillInterest",
                label: "Competencies",
                options: DropdownListData.generateOptionListJson(commonData.skills),
                narrow: true
            },
            status: {
                id: "status",
                label: "Status",
                options: DropdownListData.generateOptionListJson(commonData.status),
                narrow: true
            },
            mobility : getDefaultTemplate("location"),

            company: {
                id: "company",
                label : "Company",
                narrow : true,
                options : DropdownListData.generateOptionListJson(commonData.company)
            },
            location : getDefaultTemplate("location"),
            industrialPharmacist : {
                label : "Industrial pharmacist",
                options :[
                    {name : "industrialPharmacist",id: "industrialPharmacist",value:"1",label:"Yes"},
                    {name : "industrialPharmacist",id: "industrialPharmacistNo",value:"0",label:"No",selected:true},
                    
                ]
            }
        }
        tpl.mobility.label = "Mobility";
        tpl.mobility.narrow = true;

        tpl.location.label = "Location";
        tpl.location.narrow = true;
        return tpl;
    }

    


    var userTypePickerTplData = {
        userTypes: [
        ]
    }
    var userSortTplData = {
        userSort: {
            id: "userSort",
            options: [
                { option: "Relevance", value: 1 },
                { option: "Years of experience", value: 2 },
                { option: "Seniority", value: 3 },
                { option: "Availability (closest to furthest", value: 4 }
            ]
        }
    }
    $.each(commonData.roles, function (k, v) {
        userTypePickerTplData.userTypes.push({
            value: k,
            name: v.name
        });
    });
    function loadUserList(success,fail){
        function addListToCache(list,userLevel){
            user["userList"+userLevel] = list;
            //console.log(user);
        }
        modAmount = 4;
        loaded = 0;
        for(var i=1;i<=modAmount;++i){
            User.getUserList(i,addListToCache)
            .done(function(){
                ++loaded;
                if(loaded == modAmount){
                    if(success){
                        console.log(user);
                        success();
                    }
                }
            })
            .fail(function(){
                if(fail){
                    fail();
                }
            });
        }
    }
    function rolePicker() {
        $("#userType").on("change", function () {
            var role = getVal("userType");
            
        })
    }
    function renderUserList(list){

        var act = function(list){
            var html = "";
            $.each(list,function(k,v){
                html += $.render.userListItem(v.getShortData());
            })
            $("#userList").html(html);
        }
        act(list);
        //User.getUserList(1,act);
        //User.getUserList(2,act);
        //User.getUserList(3,act);
    }
    function applyCriteria(){
        var crit = new UserCriteria().fromForm();
        var role = parseInt(getVal("userType"));
        var tbu = user["userList"+role];

        var userList = crit.processUserList(tbu);
        renderUserList(userList);
        //console.log(userList);
        
    }
    function renderUserCriteria(userLevel) {
        var tpl = getUserCriteriaTplData();
        tpl.userListLevel = userLevel;
        
        DropdownListData.toForm(userLevel,tpl.userType);
        var html = $.render.userCriteria(tpl);

        $("#userCriteria").html(html);
        $(".selectpicker").selectpicker("refresh");
        $(".datepicker").datepicker({});
        for(var field of UserCriteria.getFields()){
            $("#"+field).change(function(){
                applyCriteria();
            });
        }
        regionPicker("country","region");
        $("#keyword").off().on("input",function(){
            applyCriteria();
        })
        $("#userType").off().change(function(){
            applyCriteria();
            renderUserCriteria(getVal("userType"));
        })
    }
    function renderUserTypePicker() {
        $("#mainContainer").html($.render.userTypePicker(userTypePickerTplData));
    }
    function renderUserListPage() {
        $("#mainContainer").html($.render.userList(userSortTplData));
    }
    function controller() {
        ajaxGet("api/EmployeeDepartmentsAPI?email="+user.mailAddress)
        .done(function(data){
            //console.log(data);
        });
        var role = sessionData.location[1];
        switch (role) {

            case "1": case "2": case "3": case "4":
                if(user.userList1 && user.userList2 && user.userList3 && user.userList4){
                    renderUserListPage();
                    renderUserCriteria(role);
                    applyCriteria()
                } else {
                    var cb = function(){
                        controller();
                    }
                    var failure = function(){
                        alert("A critical error occured, loading user lists failed");
                    }
                    loadUserList(cb,failure);

                }
                
                break;
            case "pick": default:
                loadUserList();
                renderUserTypePicker();
                break;
        }
    }

    controller();
}