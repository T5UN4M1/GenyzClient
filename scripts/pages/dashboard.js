function dashboardView() {
    var items = [];
    console.log(user.userLevel);
    switch (user.userLevel) {
        case 1: // admin
            items = [
                {
                    "iconSrc": "./Assets/Icons/userlist.png",
                    "link": "userList",
                    "linkText": lang[sessionData["lang"]]["viewUserList"]
                },
                {
                    "iconSrc": "./Assets/Icons/project.png",
                    "link": "projectlist-all",
                    "linkText": lang[sessionData["lang"]]["viewProjectList"]
                },
                {
                    "iconSrc": "./Assets/Icons/bookmarkApplication.png",
                    "link": "admin-bookmark",
                    "linkText": lang[sessionData["lang"]]["reviewBookmarksAndApplications"]
                },
                {
                    "offsetIt":true,
                    "iconSrc": "./Assets/Icons/manageCompetencies.png",
                    "link": "admin-suggestions",
                    "linkText": lang[sessionData["lang"]]["manageDepartmentsAndCompetenciesSuggestions"]
                },
                {
                    "iconSrc": "./Assets/Icons/generateSubscriptionLink.png",
                    "link": "admin:linkgen",
                    "linkText": lang[sessionData["lang"]]["generateSubscriptionLink"]
                },
            ]
            break;
        case 2: // client
            items = [
                {
                    "iconSrc": "./Assets/Icons/profileEdit.png",
                    "link": "editprofile",
                    "linkText": lang[sessionData["lang"]]["editMyProfile"]
                },

                {
                    "iconSrc": "./Assets/Icons/project.png",
                    "link": "myProjects-list",
                    "linkText": lang[sessionData["lang"]]["manageMyProjects"]
                },
                {
                    "iconSrc": "./Assets/Icons/userlist.png",
                    "link": "userlist-consultants",
                    "linkText": lang[sessionData["lang"]]["viewConsultantList"]
                },
            ]
            break;
        case 3: // consultant
            items = [
                {
                    "iconSrc": "./Assets/Icons/profileEdit.png",
                    "link": "profileEdition-profile",
                    "linkText": lang[sessionData["lang"]]["editMyCvAndProfile"]
                },
                {
                    "iconSrc": "./Assets/Icons/project.png",
                    "link": "projectlist-active",
                    "linkText": lang[sessionData["lang"]]["lookForProjects"]
                },
                {
                    "iconSrc": "./Assets/Icons/userlist.png",
                    "link": "userlist-consultants:candidats",
                    "linkText": lang[sessionData["lang"]]["viewConsultantAndCandidateList"]
                },
            ]
            break;
        case 4: // candidate
            items = [
                {
                    "iconSrc": "./Assets/Icons/profileEdit.png",
                    "link": "profileEdition-profile",
                    "linkText": lang[sessionData["lang"]]["editMyCvAndProfile"]
                },
                {
                    "iconSrc": "./Assets/Icons/project.png",
                    "link": "projectlist-active",
                    "linkText": lang[sessionData["lang"]]["lookForProjects"]
                },
                {
                    "iconSrc": "./Assets/Icons/userlist.png",
                    "link": "userlist-consultants",
                    "linkText": lang[sessionData["lang"]]["viewConsultantList"]
                },
            ]

            break;
        default : 
            console.log(user);
            console.log("Error, trying to get user dashboard with ID :" + user.userLevel);
    }
    //alert(JSON.stringify(items));
    //alertSessionData();
    var dashboardData = {
        items : items
    }
    $("#mainContainer").html($.render.dashboard(dashboardData));
/*
    var template = $.templates("#dashboardViewTpl");
    var htmlOutput = template.render(data);
    $("#mainContainer").html(htmlOutput);*/
    $("img.dashboardItem").hover(function () {
        $(this).fadeOut(250).fadeIn(250);
    });

}
