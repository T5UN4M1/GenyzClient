function myProjects() {
    //console.log(JSON.stringify(commonData));

    function retrieveAddProjectFormData(){
        return {
            "Name" : $("#projectName").val(),
            "Description" : $("#projectDescription").val(),
            "RequiredProfile" : $("#projectProfileNeeded").val(),
            "StartingDate" : dateFormToDateTime("projectStartDate"),
            "Duration" : $("#projectDuration").val() + "",
            "DeadLineCV" : dateFormToDateTime("projectDeadline"),
            "IdStatus" : 0,
            "IdDepartment": $("#projectDepartment").val(),
            "IdBudget": $("#projectBudget").val(),
            "IdArea": $("#projectArea").val(),
            "lstSkills" : $("#projectSkills").val()
        };
    }
    function retrieveProjectCriterias(){
        return {
            "Name": $("#projectName").val(),
            "Duration" : $("#projectDuration").val() + "",
            "IdStatus" : $("#projectStatus").val(),
            "IdDepartment": $("#projectDepartment").val(),
            "lstSkills" : $("#projectSkills").val(),
            "IdLanguages" : $("#projectLanguages").val(),
            "IdBudget": $("#projectBudget").val(),
            "StartsDate" : {
                date : dateFormToDateTime("projectStartDate"),
                side : $("#projectStartDate").val()
            },
            "EndsDate" : {
                date : dateFormToDateTime("projectEndDate"),
                side : $("#projectEndDate").val()
            }
        };
    }
    switch (sessionData.location[1]) {
        default: case "home":// vue liste des projets, boutton selection de critères et boutton "ajouter projet"
            Project.prototype.getProjectsFromServer().done(function (data) {

                currentCustomerProjects = [];
                currentCustomerProjects = Project.prototype.parseProjectsFromServer(data);

                var sortedCustomerProjects = currentCustomerProjects;
                console.log(currentCustomerProjects);

                //currentCustomerProjects = data;
                console.log("crits:"+JSON.stringify(currentProjectCriterias));
                //console.log("DATA:"+JSON.stringify(commonData));

                function applyCriterias(){
                    var crits = new ProjectCriteria();
                    sortedCustomerProjects = [];
                    crits.createFromFormData();
                    currentProjectCriterias = crits;
                    if(typeof currentProjectCriterias.isValid == "function"){
                        for(var k in currentCustomerProjects){
                            if(currentProjectCriterias.isValid(currentCustomerProjects[k])){
                                sortedCustomerProjects.push(currentCustomerProjects[k]);
                            }
                        }
                        //console.log("Remaining projects :" + JSON.stringify(currentCustomerProjects));
                        var projects = [];
                        for(var project of sortedCustomerProjects){
                            if(!project){
                                continue;
                            }
                            projects.push(project.getShortData());
                        }
                        
                        projectListData.projects = projects;
                        var list = "";
                        for(var proj of projects){
                            list += $.render.projectListItem(proj);
                        }
                        $("#projectListContainer").html(list);
                        console.log("List",projectListData);
                    } else {
                        console.log(currentProjectCriterias);
                        //alert("no criteria");
                    }
                //data = applyCriteriasOnProjectList(data,currentProjectCriterias);
                //console.log(JSON.stringify(data));
                //console.log(JSON.stringify(commonData));
                }

                var projectListData = {
                    buttons: [
                        {
                            id: "newProject",
                            label: "Add a new project"
                        },
                        /*{
                            id: "changeCriterias",
                            label: "Change criterias"
                        }*/
                    ],
                    criterias: [
                        //{ criteria: "Department:Pharmaceutic" },
                        //{ criteria: "Start date:before 15/06/2018" }
                    ],
                    projects: [
                        
                    ]

                }
                var projects = [];
                for(var project of sortedCustomerProjects){
                    if(!project){
                        continue;
                    }
                    projects.push(project.getShortData());

                }
                projectListData.projects = projects;
                if(typeof currentProjectCriterias.getCriteriaDisplayJson == "function"){
                    projectListData.criterias = currentProjectCriterias.getCriteriaDisplayJson();
                } else {
                    projectListData.criterias = [{criteria : "No criteria"}];
                }


                /////////////////////

                var projectCriteriaData = {
                    projectCriteriaSelection: "Project criteria selection",
                    projectName: getDefaultTemplate("projectName"),
                    projectDuration: getDefaultTemplate("projectDuration"),
                    projectStatus: getDefaultTemplate("projectStatus"),
                    projectDepartments: getDefaultTemplate("projectDepartments"),
                    projectSkills: getDefaultTemplate("projectSkills"),
                    projectLanguages: getDefaultTemplate("projectLanguages"),
                    projectBudgets: getDefaultTemplate("projectBudgets"),
                    projectStarts: getDefaultTemplate("projectStarts") ,
                    projectEnds: getDefaultTemplate("projectEnds"),
                    criteriaSubmit: {
                        id: "criteriaSubmit",
                        label: "Search"
                    }
    
                };
                projectListData["criterias"] = projectCriteriaData;
                projectListData.criterias.projectStatus.narrow = true;
                projectListData.criterias.projectDepartments.narrow = true;
                projectListData.criterias.projectSkills.narrow = true;
                projectListData.criterias.projectLanguages.narrow = true;
                projectListData.criterias.projectBudgets.narrow = true;
                projectListData.criterias.projectName.narrow = true;
                projectListData.criterias.projectDuration.narrow = true;

                console.log(projectListData);

                ///////////////////////
                $("#mainContainer").html($.render.projectList(projectListData));
                 $('.selectpicker').selectpicker('refresh');
                var toBeChecked = [
                    "projectDepartment",
                    "projectStatus",
                    "projectSkills",
                    "projectLanguages",
                    "projectBudget"
                ];
                var TBC2 = [
                    "projectName",
                    "projectDuration"
                ];
                for(var v of toBeChecked){
                    $("#"+ v).change(function(){
                        applyCriterias();
                        console.log(sortedCustomerProjects);
                    })
                }
                for(var v of TBC2){
                    $("#"+ v).on('input',function(){
                        
                        applyCriterias();
                        console.log(sortedCustomerProjects);
                    })
                }
               


                clickToLocation("#newProject","#myProjects-add");
                clickToLocation("#changeCriterias","#myProjects-crit");
            });

            break;
        case "add":
            var projectAddData = $.extend({},Project.prototype.getDefaultTplData(),{

                submitDraft: {
                    id: "submitDraft",
                    label: "Submit draft"
                },
                submitToValidation: {
                    id: "submitToValidation",
                    label: "Submit to validation"
                },

                nextPage: {
                    id : "nextPage",
                    label : "Next"
                },
                precedentPage: {
                    id: "precedentPage",
                    label: "Back"
                }
            });
            
        
            console.log(projectAddData);
            projectAddData.projectSkills.options = DropdownListData.generateOptionListJson(commonData.skills);
            //projectAddData.projectLanguages.options = DropdownListData.generateOptionListJson(commonData.languages);
            projectAddData.projectArea.options = DropdownListData.generateOptionListJson(commonData.areas);
            projectAddData.projectBudget.options = DropdownListData.generateOptionListJson(commonData.budgets);
            projectAddData.projectDepartment.options = DropdownListData.generateOptionListJson(commonData.departments);
            console.log(projectAddData)
            $("#mainContainer").html($.render.projectAdd(projectAddData));
            
            $("#projectStartDate").datepicker({});
            $("#projectCVDeadline").datepicker({});
            $("#page2").hide();
            $("#nextPage").click(function(){
                $("#page1").hide();
                $("#page2").show();
            });
            $("#precedentPage").click(function(){
                $("#page2").hide();
                $("#page1").show();
            });

            //fillDate("projectStartDate", 0, 30);
            //fillDate("projectDeadline", 0, 30);
            // slider
            
            var sliderValues = [1, 2, 3, 4, 5, 6, 8, 9, 10, 12, 15, 18, 24, 30, 36, 42, 48, 54, 60];
            $('#projectDurationSlider').bootstrapSlider({
                max: sliderValues.length - 1,
                min: 0,
                step: 1,
                formatter: function (val) {
                    return sliderValues[val];
                }
            }).on('change', function (data) {
                $("#projectDuration").val(sliderValues[data.value.newValue])
            });
            $('.selectpicker').selectpicker('refresh');
            activateLanguageSelection();
            $("#submitToValidation").click(function(){
                
                //var data = retrieveAddProjectFormData();
                //data.IdStatus = 2; // Approval
                var project = new Project();
                project.createFromFormData();
                project.setStatus(2);
                project.sendCreateProjectRequest()
                /*
                alert(JSON.stringify(data));
                ajaxPost("api/projectsApi",data)*/
                .done(function(data){
                    alert(JSON.stringify(data));
                    alert("project inserted");
                }).fail(function(data){
                    alert(JSON.stringify(data));
                    alert("didnt work");;
                })
            });
            
            break;
        case "view":
            var projectId = sessionData.location[2];
            var project = {};
            Project.prototype.getProjectsFromServer(projectId)
            .done(function(data){
                var project = {};
                console.log(data);
                project = Project.prototype.parseProjectsFromServer(data);
                /*for(var p of projects){
                    if(p.id == projectId){
                        project = p;
                        //alert("found project");
                        break;
                    }
                }*/

            console.log("projectData:"+JSON.stringify(project));
            projectViewData = project.getLongData();
            $("#mainContainer").html($.render.projectView(addLangToData(projectViewData)));                
            }).fail(function(data){
            });



            
            break;
        case "del":
            $("#mainContainer").html($.render.projectDeletion(addLangToData({})));

            break;
        case "edit":
            var projectEditData = $.extend({},Project.prototype.getDefaultTplData(),{


                submitToValidation: {
                    id: "saveChanges",
                    label: "Save changes"
                },

                nextPage: {
                    id : "nextPage",
                    label : "Next"
                },
                precedentPage: {
                    id: "precedentPage",
                    label: "Back"
                }
            });
            projectEditData.projectSkills.options = DropdownListData.generateOptionListJson(commonData.skills);
            projectEditData.projectLanguages.options = DropdownListData.generateOptionListJson(commonData.languages);
            projectEditData.projectArea.options = DropdownListData.generateOptionListJson(commonData.areas);
            projectEditData.projectBudget.options = DropdownListData.generateOptionListJson(commonData.budgets);
            projectEditData.projectDepartment.options = DropdownListData.generateOptionListJson(commonData.departments);

            var projectId = sessionData.location[2];
            var project = new Project();
            Project.prototype.getProjectsFromServer()
            .done(function(data){
               
                var projects = [];
                projects = Project.prototype.parseProjectsFromServer(data); console.log(projects);
                for(var p of projects){
                    if(p.id == projectId){
                        project = p;
                        break;
                    }
                }
                console.log(project);
                project.injectDataIntoForm(projectEditData);
                console.log(projectEditData);
                $("#mainContainer").html($.render.projectAdd(projectEditData));


                $("#projectStartDate").datepicker({});
                $("#projectCVDeadline").datepicker({});
                $("#page2").hide();
                $("#nextPage").click(function(){
                    $("#page1").hide();
                    $("#page2").show();
                });
                $("#precedentPage").click(function(){
                    $("#page2").hide();
                    $("#page1").show();
                });

                var sliderValues = [1, 2, 3, 4, 5, 6, 8, 9, 10, 12, 15, 18, 24, 30, 36, 42, 48, 54, 60];
                $('#projectDurationSlider').bootstrapSlider({
                    max: sliderValues.length - 1,
                    min: 0,
                    step: 1,
                    formatter: function (val) {
                        return sliderValues[val];
                    }
                }).on('change', function (data) {
                    $("#projectDuration").val(sliderValues[data.value.newValue])
                });
                $('.selectpicker').selectpicker('refresh');


                $("#saveChanges").click(function(){
                    
                    //var data = retrieveAddProjectFormData();
                    //data.IdStatus = 2; // Approval
                    //var project = new Project();
                    project.createFromFormData(true);
                    //project.setStatus(2);
                    project.sendEditProjectRequest()
                    /*
                    alert(JSON.stringify(data));
                    ajaxPost("api/projectsApi",data)*/
                    .done(function(data){
                        alert(JSON.stringify(data));
                        alert("JESUS CHRIST IT WORKS")
                    }).fail(function(data){
                        alert(JSON.stringify(data));
                        alert("aw shiet");
                    })
                });
            });
            break;
        case "crit":
            var projectCriteriaData = {
                projectCriteriaSelection: "Project criteria selection",
                projectName: getDefaultTemplate("projectName"),
                projectDuration: getDefaultTemplate("projectDuration"),
                projectStatus: getDefaultTemplate("projectStatus"),
                projectDepartments: getDefaultTemplate("projectDepartments"),
                projectSkills: getDefaultTemplate("projectSkills"),
                projectLanguages: getDefaultTemplate("projectLanguages"),
                projectBudgets: getDefaultTemplate("projectBudgets"),
                projectStarts: getDefaultTemplate("projectStarts") ,
                projectEnds: getDefaultTemplate("projectEnds"),
                criteriaSubmit: {
                    id: "criteriaSubmit",
                    label: "Search"
                }

            };
            $("#mainContainer").html($.render.projectCriteriaSelection(addLangToData(projectCriteriaData)));
            fillDate("projectStarts", 30, 30);
            fillDate("projectEnds", 30, 30);
            var sliderValues = [1, 2, 3, 4, 5, 6, 8, 9, 10, 12, 15, 18, 24, 30, 36, 42, 48, 54, 60];
            $('#projectDurationSlider').bootstrapSlider({
                max: sliderValues.length - 1,
                min: 0,
                step: 1,
                formatter: function (val) {
                    return sliderValues[val];
                }
            }).on('change', function (data) {
                $("#projectDuration").val(sliderValues[data.value.newValue])
            });
            
            $('.selectpicker').selectpicker('refresh');
            $("#criteriaSubmit").click(function(){
               // var crit = retrieveProjectCriterias();

                /*var testCriteria = new ProjectCriteria();
                testCriteria.createFromFormData();
                var projectss = [];
                for(var p of currentCustomerProjects){
                    var projj = new Project();
                    projj.createFromServerData(p);
                    if(testCriteria.isValid(projj)){
                        console.log("Criteria test : "+JSON.stringify(projj) + " is considered valid with crits : " + JSON.stringify(testCriteria));
                    } else {
                        console.log("Criteria test : "+JSON.stringify(projj) + " is considered non valid with crits : " + JSON.stringify(testCriteria));
                    }
                    projectss.push(projj);
                }*/
                
                var crit = new ProjectCriteria();
                crit.createFromFormData();

                console.log(crit);
                currentProjectCriterias = crit;
                location.hash = "myProjects-home";
            });
            break;
    }
}